import java.io.*;
import java.rmi.AccessException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

public class RMIServer extends UnicastRemoteObject implements
		RMIServerInterface {
	private static final long serialVersionUID = 1L;
	private static ArrayList<String> onlineUsers = new ArrayList<String>();
	public static ArrayList<String> serverAddress = new ArrayList<String>();
	static java.sql.Connection connection;
	static Statement statement;
	static PreparedStatement reg, getLog, getMeetNotif, getActionNotif,
			getGroupNotif, addMeet, addItem, addUserMeet, modifItem, removItem;
	static PreparedStatement addAct, addDec;
	static PreparedStatement getAllUsers;

	static PreparedStatement getCurrMeets, getCurrMeet, getMeetUsers,
			getMeetItems, getMeetItem, getChat, getItemName, getDec,
			getUserMeets, getUpMeets, getMeetCrono, getUserAct;

	static PreparedStatement actDone, getStatus, addChat, updateMeet,
			updateAct, getActs;

	static PreparedStatement addGroup, addUserGroup, getGroupUsers,
			getAllGroups, getUserGroups, getCurrGroup, updateGroup,
			declineGroup;

	protected RMIServer() throws RemoteException {
		super();

	}

	public void print_on_RMIServer(String s) throws RemoteException {
		System.out.println("> " + s);
	}

	public String becomeOnline(int userID) throws RemoteException {
		String notif = "";
		boolean check = false;
		int id;
		try {
			getMeetNotif.setInt(1, userID);
			ResultSet resultset = getMeetNotif.executeQuery();
			while (resultset.next()) {
				if (!check) {
					check = true;
					notif += "\n**Notifications**\n";
				}
				id = resultset.getInt(1);
				notif += "You were invited to Meeting [" + id + " -> Title: "
						+ resultset.getString(2) + "]\n";

				updateMeet.setInt(1, 1);
				updateMeet.setInt(2, userID);
				updateMeet.setInt(3, id);
				updateMeet.executeUpdate(); // 0 -> 1, agora já viu a notif

				connection.commit();
			}
			getActionNotif.setInt(1, userID);
			resultset = getActionNotif.executeQuery();
			while (resultset.next()) {
				if (!check) {
					check = true;
					notif += "\n**Notifications**\n";
				}

				id = resultset.getInt(1);
				notif += "You were given the Action [" + id + " -> Meeting: "
						+ resultset.getString(3) + ", Item: "
						+ resultset.getString(4) + "]: "
						+ resultset.getString(5) + "\n";
				updateAct.setInt(1, 1);
				updateAct.setInt(2, id);
				updateAct.executeUpdate(); // 0 -> 1, agora já viu a notif

				connection.commit();
			}
			getGroupNotif.setInt(1, userID);
			resultset = getGroupNotif.executeQuery();
			while (resultset.next()) {
				if (!check) {
					check = true;
					notif += "\n**Notifications**\n";
				}
				id = resultset.getInt(1);
				notif += "You were invited to Group [" + id + " -> Name: "
						+ resultset.getString(2) + "]\n";

				updateGroup.setInt(1, 1);
				updateGroup.setInt(2, userID);
				updateGroup.setInt(3, id);
				updateGroup.executeUpdate(); // 0 -> 1, agora já viu a notif
			}

			connection.commit();
			return "ok#" + notif;

		} catch (SQLException ex) {

			doRollback();
			System.out.println("[SQL] Something went wrong:  becomeOnline(): "
					+ ex);
			return "err#[SQL] Something went wrong:  becomeOnline()";
		}
	}

	public String becomeOffline(String username) throws RemoteException {
		synchronized (onlineUsers) {
			onlineUsers.remove(username);
			return "ok#";
		}
	}

	public static void main(String[] args) throws AccessException,
			RemoteException {
		loadProperties();
		System.setProperty("java.rmi.server.hostname", serverAddress.get(0));
		RMIServerInterface s = new RMIServer();
		LocateRegistry.createRegistry(1099).rebind("RMIserver1", s);

		try {
			connection = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/BD_Project", "RMI", "8eQLmRem5cyrE3GL");
			connection.setAutoCommit(false); // To allow secure transactions :)

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.exit(0);
		}
		System.out.println("Server RMI ready...");

		try {
			reg = connection
					.prepareStatement("insert into user (name, email, username, password) values (?,?,?,?)");

			getLog = connection
					.prepareStatement("select userid, username, password from user where username =? and password=?");
			getMeetNotif = connection
					.prepareStatement("select meetingid, title from meeting where meetingid = (select meetingid from user_meeting where userid=? and status='0')");
			getActionNotif = connection
					.prepareStatement("select actionid, action.itemid, meets, items, task from action, (select title meets, itemname items, itemid from item, meeting where meeting.meetingid=item.meetingid) u_a where u_a.itemid=action.itemid and userid=? and status='0'");
			getGroupNotif = connection
					.prepareStatement("select groupid, name from `group` where groupid = (select groupid from user_group where userid=? and status='0')");
			addGroup = connection.prepareStatement(
					"insert into `group` (name, creator) values (?,?)",
					Statement.RETURN_GENERATED_KEYS);
			addUserGroup = connection
					.prepareStatement("insert into user_group (userid,groupid,status) values (?,?,?)");

			addMeet = connection
					.prepareStatement(
							"insert into meeting (title, outcome, starttime, endtime, location, creator) values (?,?,?,?,?,?)",
							Statement.RETURN_GENERATED_KEYS);
			addItem = connection
					.prepareStatement("insert into item (meetingid, itemname, creator) values (?,?,?)");
			addUserMeet = connection
					.prepareStatement("insert into user_meeting (userid, meetingid, status) values (?,?,?)");

			modifItem = connection
					.prepareStatement("update item set itemname=? where itemid =? and itemname!='Any other business'");
			removItem = connection
					.prepareStatement("delete from item where itemid=? and itemname!= 'Any other business'");

			addAct = connection
					.prepareStatement("insert into action (userid, itemid, task, status) values (?,?,?,?)");
			addDec = connection
					.prepareStatement("insert into decision (itemid, description) values (?,?)");

			getAllGroups = connection
					.prepareStatement("select groupid, name, creator from `group`");
			getAllUsers = connection
					.prepareStatement("select userid,username from user where userid!=?");
			getCurrMeets = connection
					.prepareStatement("select meetingid, title, outcome, starttime, endtime, location from meeting where starttime<=? and endtime>=? "
							+ "and meetingid in (select meetingid from user_meeting where userid=? and status=?) order by starttime");
			getCurrMeet = connection
					.prepareStatement("select title, outcome, starttime, endtime, location, creator from meeting where meetingid=?");
			getMeetUsers = connection
					.prepareStatement("select user.userid, username, cenas from user, (select userid, status cenas from user_meeting where meetingid=?) m_u where user.userid=m_u.userid");
			getCurrGroup = connection
					.prepareStatement("select name, creator from `group` where groupid=?");
			getGroupUsers = connection
					.prepareStatement("select user.userid, username, cenas from `user`, (select userid, status cenas from user_group where groupid=?) g_u where user.userid=g_u.userid");
			getMeetItems = connection
					.prepareStatement("select itemid, creator, itemname from item where meetingid=?");

			getChat = connection
					.prepareStatement("select time, message from chat where itemid=?");
			getItemName = connection
					.prepareStatement("select itemname from item where itemid=?");
			getDec = connection
					.prepareStatement("select description from decision where itemid=?");

			getUserMeets = connection
					.prepareStatement("select cenas, meeting.meetingid, title, outcome, starttime, endtime, location from meeting, (select meetingid, status cenas from user_meeting where userid=?) u_m where meeting.meetingid=u_m.meetingid order by starttime");
			getUpMeets = connection
					.prepareStatement("select cenas, meeting.meetingid, title, outcome, starttime, endTime, location from meeting, (select meetingid, status cenas from user_meeting where userid=?) u_m where meeting.meetingid=u_m.meetingid and starttime>=?");
			getUserGroups = connection
					.prepareStatement("select cenas, group.groupid, name, creator from `group`, (select groupid, status cenas from `user_group` where userid=?) u_g where group.groupid=u_g.groupid");
			getMeetCrono = connection
					.prepareStatement("select starttime, endtime from meeting where meetingid=?");

			getUserAct = connection
					.prepareStatement("select actionid, action.itemid, meets, items, task, status from action, (select title meets, itemname items, itemid from item, meeting where meeting.meetingid=item.meetingid) u_a where u_a.itemid=action.itemid and userid=?");
			actDone = connection
					.prepareStatement("update action set status='2' where actionid=?");
			getActs = connection
					.prepareStatement("select actionid, usernames, items, task, status from action, (select itemname items, itemid from item where itemid=?) i_a, (select username usernames, userid from user) u where i_a.itemid=action.itemid and action.userid=u.userid");
			getStatus = connection
					.prepareStatement("select status from user_meeting where userid=? and meetingid=?");

			addChat = connection
					.prepareStatement("insert into chat (itemid, time, message) values (?, ?, ?)");
			updateAct = connection
					.prepareStatement("update action set status=? where actionid =?");
			updateMeet = connection
					.prepareStatement("update user_meeting set status=? where userid =? and meetingid=?");
			updateGroup = connection
					.prepareStatement("update user_group set status=? where userid =? and groupid=?");
			declineGroup = connection
					.prepareStatement("delete from user_group where userid=? and groupid=?");

		} catch (SQLException e1) {
			doRollback();
			System.out.println("Something went wrong:  main(): ");
			e1.printStackTrace();
		}
	}

	private static void loadProperties() {

		InputStream in = null;
		try {
			Properties properties = new Properties();
			in = new FileInputStream("app.properties");
			properties.load(in);

			serverAddress.add(properties.getProperty("server1.address"));
			// serverAddress.add(properties.getProperty("server2.address"));

		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Something went wrong:  loadProperties(): " + e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
				}
			}
		}

	}

	/*********************************************************************************************/
	/**************************** CREDENTIALS *********************************************/
	/*********************************************************************************************/

	public String registerUser(String name, String email, String username,
			String password) throws RemoteException {
		int check;

		try {
			reg.setString(1, name);
			reg.setString(2, email.toLowerCase());
			reg.setString(3, username);
			reg.setString(4, password);

			check = reg.executeUpdate();

			if (check != 1) {
				System.out
						.println("[ERROR] Problem updating table user: More than one row inserted or none!");
				return "err#[SQL] Something went wrong:  registerUser()";
			}

			connection.commit();
			return "ok#";

		} catch (SQLException ex) {
			doRollback();
			if (ex.getMessage().contains("Duplicate"))
				return "err#Error: Credentials already in use!";

			System.out.println("[SQL] Something went wrong:  registerUser(): "
					+ ex);
			return "err#[SQL] Something went wrong:  registerUser()";
		}
	}

	public String loginUser(String username, String password)
			throws RemoteException {
		try {
			int id;
			String usrname;
			getLog.setString(1, username);
			getLog.setString(2, password);
			ResultSet resultset = getLog.executeQuery();
			if (!resultset.next())
				return "err#Error: Wrong credentials!";

			id = resultset.getInt(1);
			usrname = resultset.getString(2);

			if (!checkOnlineUser(usrname)) {
				onlineUsers.add(usrname);
				return "ok#" + id;
			}

			System.out.println("[LOGIN] Error: " + username
					+ " already online.");
			return "err#Error: " + usrname + " already online.";

		} catch (SQLException ex) {
			System.out.println("[SQL] Something went wrong:  loginUser(): "
					+ ex);
			return "err#[SQL] Something went wrong:  loginUser()";
		}

	}

	/*********************************************************************************************/
	/**************************** ADDS ****************************************************/
	/*********************************************************************************************/

	public String createGroup(String name, int userID, String creator)
			throws RemoteException {
		int check;
		int groupID;

		try {
			addGroup.setString(1, name);
			addGroup.setString(2, creator);

			check = addGroup.executeUpdate();

			if (check == 1) { // tabela group atualizada
				ResultSet generatedKeys = addGroup.getGeneratedKeys();
				if (generatedKeys.next()) {
					groupID = generatedKeys.getInt(1);
					System.out.println("Group ID = " + groupID);
					connection.commit();
					return "ok#Group created succesfully.#" + groupID;

				} else {
					doRollback();
					System.out.println("[ERROR] Problem creating group");
					return "print#Error: Problem creating group (Database access problem)!";
				}

			} else {
				doRollback();
				System.out.println("[ERROR] Problem creating group");
				return "print#Error: Problem creating group (Database access problem)!";
			}

		} catch (SQLException ex) {
			doRollback();
			System.out.println("[SQL] Something went wrong:  createGroup(): "
					+ ex);
			return "print#[SQL] Something went wrong:  createGroup()";
		}
	}

	public String addMeeting(String title, String outcome, Date startDate,
			Date endDate, String location, int userID, String creator)
			throws RemoteException, InterruptedException {
		int check;
		int meetID;

		try {
			addMeet.setString(1, title);
			addMeet.setString(2, outcome);
			java.sql.Timestamp sqlDate1 = new java.sql.Timestamp(
					startDate.getTime());
			addMeet.setTimestamp(3, sqlDate1);
			java.sql.Timestamp sqlDate2 = new java.sql.Timestamp(
					endDate.getTime());
			addMeet.setTimestamp(4, sqlDate2);
			addMeet.setString(5, location);
			addMeet.setString(6, creator);

			check = addMeet.executeUpdate();

			if (check == 1) { // tabela meeting atualizada
				ResultSet generatedKeys = addMeet.getGeneratedKeys();
				if (generatedKeys.next()) {
					meetID = generatedKeys.getInt(1);
					System.out.println("MeetID = " + meetID);

					connection.commit();

					addItem.setInt(1, meetID);
					addItem.setString(2, "Any other business");
					addItem.setString(3, creator);

					check = addItem.executeUpdate(); // tabela item atualizada

					if (check != 1) {
						System.out
								.println("[ERROR] Problem updating table item!");
						return "err#Error: Meeting could not be scheduled.";
					}

					System.out.println("[OK] Meeting scheduled succesfully.");
					connection.commit();
					return "ok#Meeting scheduled succesfully.#" + meetID; // returns
																			// meetID
				}
			}

			System.out.println("[ERROR] Problem updating table meeting");
			return "print#Error: Meeting could not be scheduled!";

		} catch (SQLException ex) {
			doRollback();
			System.out.println("[SQL] Something went wrong:  addMeeting(): "
					+ ex);
			return "print#[SQL] Something went wrong:  addMeeting()";
		}

	}

	public String addItem(int meetID, String itemName, String creator)
			throws RemoteException {
		int check;

		try {
			addItem.setInt(1, meetID);
			addItem.setString(2, itemName);
			addItem.setString(3, creator);

			check = addItem.executeUpdate(); // atualiza tabela item

			if (check != 1) {
				System.out.println("[ERROR] Problem updating table item!");
				return "print#Error: Item could not be added.";
			}

			System.out.println("[OK] Item added sucessfully.");
			connection.commit();

			return "ok#Item added sucessfully.";

		} catch (SQLException ex) {
			doRollback();
			System.out.println("[SQL] Something went wrong:  addItem(): " + ex);
			return "print#[SQL] Something went wrong:  addItem()";
		}

	}

	public String removeItem(int itemID) throws RemoteException {
		int check;

		try {
			removItem.setInt(1, itemID);

			check = removItem.executeUpdate(); // elimina row da tabela item

			if (check != 1) {
				System.out
						.println("[ERROR] Problem deleting row from table item!");
				return "print#Error: Item cannot be removed.";
			}

			System.out.println("[OK] Item removed sucessfully.");
			connection.commit();

			return "ok#Item removed sucessfully.";
		} catch (SQLException ex) {
			doRollback();
			System.out.println("[SQL] Something went wrong:  removetem(): "
					+ ex);
			return "print#[SQL] Something went wrong:  removeItem()";
		}

	}

	public String modifyItem(int itemID, String itemName)
			throws RemoteException {
		int check;

		try {
			modifItem.setString(1, itemName);
			modifItem.setInt(2, itemID);

			check = modifItem.executeUpdate(); // modifica row da tabela item

			if (check != 1) {
				System.out
						.println("[ERROR] Problem modifying row from table item!");
				return "print#Error: Item cannot be modified.";
			}

			System.out.println("[OK] Item modified sucessfully.");
			connection.commit();
			return "ok#Item modified sucessfully.";

		} catch (SQLException ex) {
			doRollback();
			System.out.println("[SQL] Something went wrong:  modifyItem(): "
					+ ex);
			return "print#[SQL] Something went wrong:  modifyItem()";
		}

	}

	public String addUser2Meeting(int meetID, int user2addID)
			throws RemoteException {
		int check;

		try {
			addUserMeet.setInt(1, user2addID);
			addUserMeet.setInt(2, meetID);
			addUserMeet.setInt(3, 0); // convida

			check = addUserMeet.executeUpdate(); // tabela user_meet atualizada

			if (check != 1)
				System.out
						.println("[ERROR] Problem updating table user_meeting!");

			connection.commit();

		} catch (SQLException ex) {
			doRollback();
			if (ex.getMessage().contains("Duplicate"))
				return "print#Error: User already invited.";

			System.out
					.println("[SQL] Something went wrong:  addUser2Meeting(): "
							+ ex);
			return "print#[SQL] Something went wrong:  addUser2Meeting(): "
					+ ex;
		}

		System.out.println("[OK] User invited.");
		return "print#User invited.";

	}

	public String addGroup2Meeting(int meetID, int group2addID)
			throws RemoteException {
		try {
			getGroupUsers.setInt(1, group2addID);
			ResultSet resultset = getGroupUsers.executeQuery();

			// select
			while (resultset.next()) {
				if (resultset.getInt(3) == 2)
					addUser2Meeting(meetID, resultset.getInt(1));
			}

			System.out.println("[AddGroup2Meeting() - OK] Group invited.");
			return "print#Group invited.";

		} catch (SQLException ex) {
			System.out
					.println("[SQL] Something went wrong:  addGroup2Meeting(): "
							+ ex);
			return "print#[SQL] Something went wrong: addGroup2Meeting()";
		}
	}

	public String addUser2Group(int groupID, int user2addID)
			throws RemoteException {
		int check;
		// 0 -> Nao viu 1-> viu 2 -> aceitou
		try {
			// insert into `user_group` (`userid`,`groupid`,`status`) values
			// (?,?,?)
			// Add information to the user_group table
			addUserGroup.setInt(1, user2addID);
			addUserGroup.setInt(2, groupID);
			addUserGroup.setInt(3, 0);

			check = addUserGroup.executeUpdate();

			// Send notification
			connection.commit();

			if (check != 1)
				System.out
						.println("[ERROR] Problem updating table user_group: More than one row inserted or none!");

			return "print#User invited to group.";

		} catch (SQLException e) {
			doRollback();
			if (e.getMessage().contains("Duplicate"))
				return "print#Error: User already invited.";

			System.out.println("[SQL] Something went wrong:  addUser2Group(): "
					+ e);
			return "print#[SQL] Something went wrong:  addUser2Group(): " + e;
		}
	}

	public String addAction2User(int itemID, int user2addID, String action)
			throws RemoteException {
		int check;

		try {
			addAct.setInt(1, user2addID);
			addAct.setInt(2, itemID);
			addAct.setString(3, action);
			addAct.setInt(4, 0); // notifica

			check = addAct.executeUpdate(); // tabela action atualizada

			if (check != 1) {
				System.out.println("[ERROR] Problem updating table action!");
				return "print#Error: Task could not be assigned.";
			}

			connection.commit();
		} catch (SQLException ex) {
			doRollback();
			System.out
					.println("[SQL] Something went wrong:  addAction2User(): "
							+ ex);
			return "[SQL] Something went wrong:  addAction2User()";
		}

		System.out.println("[OK] Task assigned successfully.");
		return "print#Task assigned successfully.";
	}

	public String addAction2Group(int itemID, int group2addID, String action)
			throws RemoteException {
		try {
			getGroupUsers.setInt(1, group2addID);
			ResultSet resultset = getGroupUsers.executeQuery();

			while (resultset.next()) {
				if (resultset.getInt(3) == 2)
					addAction2User(itemID, resultset.getInt(1), action);
			}

			System.out
					.println("[AddAction2Group() - OK] Task assigned to group successfully.");
			return "print#Task assigned to group successfully.";

		} catch (SQLException ex) {
			System.out
					.println("[SQL] Something went wrong:  addGroup2Meeting(): "
							+ ex);
			return "print#[SQL] Something went wrong: addGroup2Meeting()";
		}
	}

	public String addDecision2Item(int itemID, String decision)
			throws RemoteException {
		int check;

		try {
			addDec.setInt(1, itemID);
			addDec.setString(2, decision);

			check = addDec.executeUpdate(); // tabela decision atualizada

			if (check != 1) {
				System.out.println("[ERROR] Problem updating table decision!");
				return "print#Error: Decision could not be added";
			}
			connection.commit();
		} catch (SQLException ex) {
			doRollback();
			System.out
					.println("[SQL] Something went wrong: addDecision2Item(): "
							+ ex);
			return "print#[SQL] Something went wrong:  addDecision2Item()";
		}

		System.out.println("[OK] Decision added sucessfully.");
		return "print#Decision added sucessfully.";
	}

	/*********************************************************************************************/
	/**************************** PRINTS **************************************************/
	/*********************************************************************************************/

	public String printOnlineUsers() throws RemoteException {
		String online = "";

		for (String u : onlineUsers)
			online += u + "\n";

		return online;
	}

	public String printUsers(int userID) throws RemoteException {
		String listUsers = "";
		int len = 0;

		try {
			getAllUsers.setInt(1, userID);
			ResultSet resultset = getAllUsers.executeQuery();

			while (resultset.next()) {
				listUsers += resultset.getString(1) + " -> "
						+ resultset.getString(2) + "\n";
				len++;
			}

			System.out.println("[OK] " + len + "\n" + listUsers);
			return "print#" + len + "\n" + listUsers;

		} catch (SQLException ex) {
			System.out.println("[SQL] Something went wrong:  printUsers(): "
					+ ex);
			return "print#[SQL] Something went wrong: printUsers()";
		}
	}

	public String printAllGroups() throws RemoteException {
		String listGroups = "";
		int len = 0;

		try {
			ResultSet resultset = getAllGroups.executeQuery();

			while (resultset.next()) {
				listGroups += resultset.getString(1) + " -> Group [Name: "
						+ resultset.getString(2) + ", Creator: "
						+ resultset.getString(2) + "]\n";
				len++;
			}

			System.out.println("[OK] " + len + "\n" + listGroups);
			return "print#" + len + "\n" + listGroups;

		} catch (SQLException ex) {
			System.out
					.println("[SQL] Something went wrong:  printAllGroups(): "
							+ ex);
			return "print#[SQL] Something went wrong: printAllGroups()";
		}
	}

	public String printCurrentMeetings(int userID) throws RemoteException {
		String listMeetings = "";
		int len = 0;

		try {
			java.sql.Timestamp now = new java.sql.Timestamp(
					new Date().getTime());
			getCurrMeets.setTimestamp(1, now);
			getCurrMeets.setTimestamp(2, now);
			getCurrMeets.setInt(3, userID);
			getCurrMeets.setInt(4, 2); // so imprime aquelas que aceitou

			ResultSet resultset = getCurrMeets.executeQuery();

			while (resultset.next()) {
				listMeetings += resultset.getString(1) + " -> Meeting [Title: "
						+ resultset.getString(2) + ", Outcome: "
						+ resultset.getString(3) + ", Start Time: "
						+ resultset.getString(4) + ", End Time: "
						+ resultset.getString(5) + ", Local: "
						+ resultset.getString(6) + "]\n";
				len++;
			}

			System.out.println("[OK] " + len + "\n" + listMeetings);
			return "print#" + len + "\n" + listMeetings;

		} catch (SQLException ex) {
			System.out
					.println("[SQL] Something went wrong:  printCurrentMeetings(): "
							+ ex);
			return "print#[SQL] Something went wrong: printCurrentMeetings()";
		}
	}

	public String printCurrentMeeting(int meetID) throws RemoteException {
		String listMeeting = "";

		try {
			getCurrMeet.setInt(1, meetID);

			ResultSet resultset = getCurrMeet.executeQuery();

			while (resultset.next()) {
				listMeeting += "Title: " + resultset.getString(1)
						+ "\nOutcome: " + resultset.getString(2)
						+ "\nStart Time: " + resultset.getString(3)
						+ "\nEnd Time: " + resultset.getString(4) + "\nLocal: "
						+ resultset.getString(5) + "\nCreator: "
						+ resultset.getString(6) + "\n";
			}

			listMeeting += "\n**Users**\n" + printMeetingUsers(meetID);

			System.out.println("[PrintCurrentMeeting() - OK] " + listMeeting);
			return "print#" + listMeeting;

		} catch (SQLException ex) {
			System.out
					.println("[SQL] Something went wrong:  printCurrentMeeting(): "
							+ ex);
			return "print#[SQL] Something went wrong: printCurrentMeeting()";
		}
	}

	public String printDetailedGroup(int groupID) throws RemoteException {
		String listGroup = "";

		try {
			getCurrGroup.setInt(1, groupID);

			ResultSet resultset = getCurrGroup.executeQuery();

			while (resultset.next()) {
				listGroup += "Name: " + resultset.getString(1) + "\nCreator: "
						+ resultset.getString(2) + "\n";
			}

			listGroup += "\n**Users**\n" + printGroupUsers(groupID);

			System.out.println("[PrintDetailedGroup() - OK] " + listGroup);
			return "print#" + listGroup;

		} catch (SQLException ex) {
			System.out
					.println("[SQL] Something went wrong:  printDetailedGroup(): "
							+ ex);
			return "print#[SQL] Something went wrong: printDetailedGroup()";
		}
	}

	public String printMeetingItems(int meetID) throws RemoteException {
		String listItems = "";
		int len = 0;

		try {
			getMeetItems.setInt(1, meetID);

			ResultSet resultset = getMeetItems.executeQuery();

			while (resultset.next()) {
				listItems += resultset.getString(1) + " -> [Added by "
						+ resultset.getString(2) + "] "
						+ resultset.getString(3) + "\n";
				len++;
			}

			System.out.println("[PrintMeetingItems() - OK] " + listItems);
			return "print#" + len + "\n" + listItems;

		} catch (SQLException ex) {
			System.out
					.println("[SQL] Something went wrong:  printCurrentMeeting(): "
							+ ex);
			return "print#[SQL] Something went wrong: printCurrentMeeting()";
		}
	}

	public String printChat(int itemID) throws RemoteException {
		String listChat = "";

		try {
			getChat.setInt(1, itemID);
			getItemName.setInt(1, itemID);

			ResultSet resultset = getChat.executeQuery();
			ResultSet resultSet = getItemName.executeQuery();

			while (resultset.next()) {
				listChat += "[" + resultset.getString(1) + "] "
						+ resultset.getString(2) + "\n";
			}

			if (listChat.isEmpty())
				listChat += "Nothing to display.\n";

			if (resultSet.next()) {
				System.out.println("[PrintChat() - OK]");
				return "print#[" + resultSet.getString(1) + "] Chatroom\n\n"
						+ listChat;
			}

			System.out.println("[PrintChat - Error]");
			return "print#Something went wrong: printChat()";

		} catch (SQLException ex) {
			System.out.println("[SQL] Something went wrong:  printChat(): "
					+ ex);
			return "print#[SQL] Something went wrong: printChat()";
		}

	}

	public String printUserMeetings(int userID) throws RemoteException {
		String listMeetings = "", status;
		int len = 0;
		int value;

		try {
			getUserMeets.setInt(1, userID);

			ResultSet resultset = getUserMeets.executeQuery();

			while (resultset.next()) {

				value = resultset.getInt(1);

				if (value == 2)
					status = "[Accepted] ";
				else {
					if (value == 3)
						status = "[Declined] ";

					else {
						if (value == 0) {
							updateMeet.setInt(1, 1); // 1, agora já viu a notif
							updateMeet.setInt(2, userID);
							updateMeet.setInt(3, resultset.getInt(2));

							updateMeet.executeUpdate();
							connection.commit();
						}
						status = "[Invited] ";
					}
				}

				listMeetings += status + resultset.getString(2)
						+ " -> Meeting [Title: " + resultset.getString(3)
						+ ", Outcome: " + resultset.getString(4)
						+ ", Start Time: " + resultset.getString(5)
						+ ", End Time: " + resultset.getString(6) + ", Local: "
						+ resultset.getString(7) + "]\n";
				len++;
			}

			System.out.println("[PrintUserMeetings() - OK] " + listMeetings);
			return "print#" + len + "\n" + listMeetings;

		} catch (SQLException ex) {
			doRollback();
			System.out
					.println("[SQL] Something went wrong:  printUserMeetings(): "
							+ ex);
			return "print#[SQL] Something went wrong: printUserMeetings()";
		}
	}

	public String printUpcomingMeetings(int userID) throws RemoteException {
		String listMeetings = "", status;
		int len = 0;
		int value;

		try {
			java.sql.Timestamp now = new java.sql.Timestamp(
					new Date().getTime());
			getUpMeets.setInt(1, userID);
			getUpMeets.setTimestamp(2, now);

			ResultSet resultset = getUpMeets.executeQuery();

			while (resultset.next()) {

				value = resultset.getInt(1);

				if (value == 2)
					status = "[Accepted] ";
				else {
					if (value == 3)
						status = "[Declined] ";

					else {
						if (value == 0) {
							updateMeet.setInt(1, 1); // 1, agora já viu a notif
							updateMeet.setInt(2, userID);
							updateMeet.setInt(3, resultset.getInt(2));

							updateMeet.executeUpdate();
							connection.commit();
						}

						status = "[Invited] ";
					}
				}

				listMeetings += status + resultset.getString(2)
						+ " -> Meeting [Title: " + resultset.getString(3)
						+ ", Outcome: " + resultset.getString(4)
						+ ", Start Time: " + resultset.getString(5)
						+ ", End Time: " + resultset.getString(6) + ", Local: "
						+ resultset.getString(7) + "]\n";
				len++;
			}

			System.out
					.println("[PrintUpcomingMeetings() - OK] " + listMeetings);
			return "print#" + len + "\n" + listMeetings;

		} catch (SQLException ex) {
			doRollback();
			System.out
					.println("[SQL] Something went wrong:  printUpcomingMeetings(): "
							+ ex);
			return "print#[SQL] Something went wrong: printUpcomingMeetings()";
		}
	}

	public String printUserGroups(int userID) throws RemoteException {
		String listGroups = "", status;
		int len = 0, value;

		try {
			getUserGroups.setInt(1, userID);

			ResultSet resultset = getUserGroups.executeQuery();

			while (resultset.next()) {

				value = resultset.getInt(1);

				if (value == 2)
					status = "[Accepted] ";
				else {
					if (value == 0) {
						updateGroup.setInt(1, 1); // 1, agora já viu a notif
						updateGroup.setInt(2, userID);
						updateGroup.setInt(3, resultset.getInt(2));
						updateGroup.executeUpdate();
						connection.commit();
					}
					status = "[Invited] ";
				}

				listGroups += status + resultset.getInt(2)
						+ " -> Group [Name: " + resultset.getString(3)
						+ ", Creator: " + resultset.getString(4) + "]\n";
				len++;
			}

			System.out.println("[PrintUserGroups() - OK] \n" + listGroups);
			return "print#" + len + "\n" + listGroups;

		} catch (SQLException ex) {
			doRollback();
			System.out
					.println("[SQL] Something went wrong:  printUserGroups(): "
							+ ex);
			return "print#[SQL] Something went wrong: printUserGroups()";
		}
	}

	public String getMeetingCrono(int meetID) throws RemoteException {
		int crono;

		try {
			getMeetCrono.setInt(1, meetID);

			ResultSet resultset = getMeetCrono.executeQuery();

			if (resultset.next()) {
				Timestamp now = new Timestamp(new Date().getTime());

				if (resultset.getTimestamp(2).before(now))
					crono = -1; // past meetings
				else {
					if (resultset.getTimestamp(1).after(now))
						crono = 1; // future meetings
					else
						crono = 0;// current meetings
				}

				System.out.println("[GetMeetingCrono() - OK] Crono = " + crono);
				return "print#" + crono;
			}

			return "print#2"; // invalid option

		} catch (SQLException ex) {
			System.out
					.println("[SQL] Something went wrong:  getMeetingCrono(): "
							+ ex);
			return "print#[SQL] Something went wrong: getMeetingCrono()";
		}
	}

	public String printUserActions(int userID) throws RemoteException {

		String listActions = "";
		String status;
		int len = 0;
		int value;

		try {
			getUserAct.setInt(1, userID);
			ResultSet resultset = getUserAct.executeQuery();

			while (resultset.next()) {

				value = resultset.getInt(6);

				if (value == 0) {
					updateAct.setInt(1, 1); // 1, agora já viu a notif
					updateAct.setInt(2, resultset.getInt(1));

					updateAct.executeUpdate();
					connection.commit();

					status = " -> NOT DONE\n";
				}

				else {
					if (value == 2)
						status = " -> DONE\n";
					else
						status = " -> NOT DONE\n";
				}

				listActions += resultset.getString(1) + " -> [Meeting: "
						+ resultset.getString(3) + ", Item: "
						+ resultset.getString(4) + "] "
						+ resultset.getString(5) + status;
				len++;
			}

			System.out.println("[PrintUserActions() - OK] " + listActions);
			return "print#" + len + "\n" + listActions;

		} catch (SQLException ex) {
			doRollback();
			System.out
					.println("[SQL] Something went wrong:  printUserActions(): "
							+ ex);
			return "print#[SQL] Something went wrong: printUserActions()";
		}
	}

	public String printActions(int itemID) throws RemoteException {
		String listActions = "";
		String status;
		int value;

		try {
			getActs.setInt(1, itemID);
			getItemName.setInt(1, itemID);

			ResultSet resultSet = getItemName.executeQuery();
			ResultSet resultset = getActs.executeQuery();

			while (resultset.next()) {

				value = resultset.getInt(5);

				if (value == 2)
					status = " -> DONE\n";

				else
					status = " -> NOT DONE\n";

				listActions += resultset.getString(1) + " -> [Item: "
						+ resultset.getString(3) + "] To "
						+ resultset.getString(2) + ": "
						+ resultset.getString(4) + status;
			}

			if (listActions.isEmpty())
				listActions += "Nothing to display.\n";

			if (resultSet.next()) {
				System.out.println("[PrintActions() - OK] " + listActions);
				return "print#[" + resultSet.getString(1) + "] Actions\n\n"
						+ listActions;
			}

			System.out.println("[PrintActions() - Error]");
			return "print#Something went wrong: PrintActions()";

		} catch (SQLException ex) {
			System.out.println("[SQL] Something went wrong:  printActions(): "
					+ ex);
			return "print#[SQL] Something went wrong: printActions()";
		}
	}

	public String printDecisions(int itemID) throws RemoteException {
		String listDecisions = "";

		try {
			getDec.setInt(1, itemID);
			getItemName.setInt(1, itemID);

			ResultSet resultset = getDec.executeQuery();
			ResultSet resultSet = getItemName.executeQuery();

			while (resultset.next()) {
				listDecisions += resultset.getString(1) + "\n";
			}

			if (listDecisions.isEmpty())
				listDecisions += "Nothing to display.\n";

			if (resultSet.next()) {
				System.out.println("[PrintDecisions() - OK]");
				return "print#[" + resultSet.getString(1)
						+ "] Key Decisions\n\n" + listDecisions;
			}

			System.out.println("[PrintDecisions() - Error]");
			return "print#Something went wrong: printDecisions()";

		} catch (SQLException ex) {
			System.out.println("[SQL] Something went wrong:  printChat(): "
					+ ex);
			return "print#[SQL] Something went wrong: printChat()";
		}
	}

	private String printMeetingUsers(int meetID) {
		String listUsers = "";
		int value;
		String status;

		try {
			getMeetUsers.setInt(1, meetID);

			ResultSet resultset = getMeetUsers.executeQuery();

			while (resultset.next()) {

				value = resultset.getInt(3);

				if (value == 2)
					status = "[Accepted] ";
				else {
					if (value == 3)
						status = "[Declined] ";

					else
						status = "[Invited] ";
				}

				listUsers += status + resultset.getString(2) + "\n";
			}

			return listUsers;

		} catch (SQLException ex) {
			System.out
					.println("[SQL] Something went wrong:  printMeetingUsers(): "
							+ ex);
			return "print#[SQL] Something went wrong: printMeetingUsers()";
		}
	}

	private String printGroupUsers(int groupID) {
		String listUsers = "";
		int value;
		String status;

		try {
			getGroupUsers.setInt(1, groupID);

			ResultSet resultset = getGroupUsers.executeQuery();

			while (resultset.next()) {

				value = resultset.getInt(3);

				if (value == 2)
					status = "[Accepted] ";
				else
					status = "[Invited] ";

				listUsers += status + resultset.getString(2) + "\n";
			}

			if (listUsers.isEmpty())
				return "\nAll users have left :(\n";

			return listUsers;

		} catch (SQLException ex) {
			System.out
					.println("[SQL] Something went wrong:  printGroupUsers(): "
							+ ex);
			return "[SQL] Something went wrong: printGroupUsers()";
		}
	}

	/*********************************************************************************************/
	/**************************** RESPONSES ***********************************************/
	/*********************************************************************************************/

	public String send2chat(int meetID, int itemID, String line)
			throws RemoteException, InterruptedException {
		int check;

		try {
			java.sql.Timestamp now = new java.sql.Timestamp(
					new Date().getTime());

			addChat.setInt(1, itemID);
			addChat.setTimestamp(2, now);
			addChat.setString(3, line);

			check = addChat.executeUpdate(); // tabela chat atualizada

			if (check != 1) {
				System.out.println("[ERROR] Problem updating table chat!");
				return "err_chat#"
						+ "Meeting already over. Type /quit to return";
			}

			System.out.println("[OK] Decision added sucessfully.");
			connection.commit();
			return "ok#Message added.";

		} catch (SQLException ex) {
			doRollback();
			System.out
					.println("[SQL] Something went wrong: send2chat(): " + ex);
			return "print#[SQL] Something went wrong:  send2chat()";
		}

	}

	public String getUserStatus(int meetID, int userID) throws RemoteException {
		try {
			getStatus.setInt(1, userID);
			getStatus.setInt(2, meetID);

			ResultSet resultset = getStatus.executeQuery();

			if (resultset.next())
				return "print#" + resultset.getString(1);

			return "print#0";

		} catch (SQLException ex) {
			System.out.println("[SQL] Something went wrong:  getUserStatus(): "
					+ ex);
			return "print#[SQL] Something went wrong: getUserStatus()";
		}
	}

	public String actionDone(int actionID) throws RemoteException {
		int check;

		try {
			actDone.setInt(1, actionID);

			check = actDone.executeUpdate(); // atualiza tabela action

			if (check == 0)
				return "print#Action already done";

			connection.commit();
			return "print#Action marked as done sucessfully";

		} catch (SQLException ex) {
			doRollback();
			System.out.println("[SQL] Something went wrong:  actionDone(): "
					+ ex);
			return "print#[SQL] Something went wrong:  actionDone()";
		}
	}

	public String acceptDeclineMeeting(int meetID, int userID, int option)
			throws RemoteException // falta
	{
		int check;

		try {
			option++;
			updateMeet.setInt(1, option);
			updateMeet.setInt(2, userID);
			updateMeet.setInt(3, meetID);

			check = updateMeet.executeUpdate(); // atualiza tabela user_meeting

			if (check == 0) {
				if (option == 2)
					return "print#This meeting had already been accepted";

				return "print#This meeting had already been declined";
			}

			connection.commit();
			return "ok#Operation done successfully";

		} catch (SQLException ex) {
			doRollback();
			System.out
					.println("[SQL] Something went wrong:  acceptDeclineMeeting(): "
							+ ex);
			return "print#[SQL] Something went wrong:  acceptDeclineMeeting()";
		}
	}

	public String acceptDeclineGroup(int groupID, int userID, int option)
			throws RemoteException {
		int check;

		try {
			option++;
			if (option == 2) {
				updateGroup.setInt(1, option);
				updateGroup.setInt(2, userID);
				updateGroup.setInt(3, groupID);

				check = updateGroup.executeUpdate(); // atualiza tabela
														// user_group

				if (check == 0)
					return "print#This group had already been accepted";
			}

			else {
				declineGroup.setInt(1, userID);
				declineGroup.setInt(2, groupID);
				declineGroup.executeUpdate();
			}

			connection.commit();
			return "ok#Operation done successfully";

		} catch (SQLException ex) {
			doRollback();
			System.out
					.println("[SQL] Something went wrong:  acceptDeclineGroup(): "
							+ ex);
			return "print#[SQL] Something went wrong:  acceptDeclineGroup()";
		}

	}

	/*********************************************************************************************/
	/******************************** AUXILIARY METHODS ******************************************/
	/*********************************************************************************************/

	private boolean checkOnlineUser(String username) {

		return onlineUsers.contains(username);
	}

	public static void doRollback() {
		try {
			// 1 - > Deu erro
			// 0 -> tudo ok;
			if (connection != null)
				connection.rollback();
			else {
				System.out.println("Connection problem");
			}

		} catch (SQLException e1) {
			System.out.println("[SQL] Something went wrong:  doRollback(): "+ e1);
		}

	}

}
