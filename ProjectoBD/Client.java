import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Pattern;


public class Client {


	private static int reconnections = 1;	
	private static ArrayList<String> serverAddress = new ArrayList<String>();
	private static boolean connectionError = false;
	private static Socket s;
	private static int end=1;
	private static DataInputStream in;
	private static DataOutputStream out;

	private static String client_name;
	private static int client_id;
	//private static Reader reader;

	private static ArrayList<String> requests = new ArrayList<String>();



	/**********************************************************************************************/
	/************************      MAIN FUNCTION    ***********************************************/
	/**********************************************************************************************/


	public static void main(String args[]){

		try{
			loadProperties();
			connectToServer();

			while(true){

				if(!connectionError){	// If there is any connection problem it won't start
					end = mainMenu(); 
					break;
				}

				connectToServer(); // Tries to connect

			}


			// Ends the application
			safeExit();

			System.out.println("Thank you for using our aplication!");

			//becomeOffline

			System.exit(0);
		}

		catch (Exception e){
			System.out.println("Something went wrong:  MAIN (): "+e);
		}


	}






	/*********************************************************************************************/
	/******************************** AUXILIARY METHODS ******************************************/
	/*********************************************************************************************/


	public static void sendToServer(String message){
		try {
			out.writeUTF("");
			out.writeUTF(message);

		}catch (Exception e){
			handleIOException(message);
		}
	}



	public static void createDataStream(){
		try {

			in = new DataInputStream(s.getInputStream());
			out = new DataOutputStream(s.getOutputStream());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			handleIOException();
		}catch(Exception e){
			handleIOException();
		}

	}



	public static void sendRequests(){  
		for(int i = 0; i < requests.size();i++){
			System.out.println("Sending: "+requests.get(i));
			sendToServer(requests.get(i));
		}		

		requests.clear();
	}




	/*** 2 tipos de IO Exceptions Handlers... ***/
	/**** um com e outro sem String ***/

	public static void handleIOException(){
		connectionError = true;
		// System.out.println("DEBUG: IOException detected by the handler :) uff..so scary!");

		while(connectionError == true){
			connectToServer();
		}

	}

	public static void handleIOException(String s){
		connectionError = true;
		// System.out.println("DEBUG: IOException detected by the handler :) uff..so scary!");
		System.out.println("Message saved: "+s);
		requests.add(s);

		while(connectionError == true){
			connectToServer();
		}

	}




	public static void warningConnectionProblem(){
		System.out.println("We are having some connection problems... try again later");

		//LOGOUT
		System.exit(0);
		//DEPOIS MELHORAR ISTO

	}


	public static void safeExit(){


		if(s != null)
			try {
				s.close();
			} catch (IOException e) {

			}

	}


	/********************/



	/**********************************************************************************************/
	/***************************      READ FILES     **********************************************/
	/**********************************************************************************************/




	public static void loadProperties(){


		InputStream in = null;
		try {
			Properties properties = new Properties();
			in = new FileInputStream("app.properties");
			properties.load(in);

			serverAddress.add(properties.getProperty("server1.address"));
			serverAddress.add(properties.getProperty("server2.address"));
			serverAddress.add(properties.getProperty("server3.address"));

		}
		catch(IOException e) {
			e.printStackTrace();
		}catch (Exception e){
			System.out.println("Something went wrong:  loadProperties(): "+e);
		}
		finally {
			if(in != null) {
				try {
					in.close();
				}
				catch(IOException e) { }
			}
		}

	}




	/*********************************************************************************************/
	/****************************     SERVER CONNECTION     **************************************/
	/*********************************************************************************************/


	public static void connectToServer(){

		//	System.out.println("Tentativa de ligaçao ao Server: "+reconnections);

		if(reconnections > 6){
			warningConnectionProblem();
		}

		int serverport = 6000;

		SocketAddress sockaddr1 = new InetSocketAddress(serverAddress.get(1), serverport);
		SocketAddress sockaddr2 = new InetSocketAddress(serverAddress.get(2), serverport);

		//System.out.println("addr 1: "+sockaddr1.toString());
		//System.out.println("addr 2: "+sockaddr2.toString());

		try {
			s = new Socket();
			s.connect(sockaddr1, 2000);

			reconnections = 1; // connection was successfull and it clears the reconnections counter.
			connectionError = false;
			createDataStream();

			//System.out.println("Estableci ligação! ;)");
			sendRequests();

		} catch (IOException e) {

			try {
				s.close();
				s = new Socket();
				s.connect(sockaddr2, 2000);

				reconnections = 1; // connection was successfull and it clears the reconnections counter.
				connectionError = false;
				createDataStream();

				//System.out.println("Estableci ligação! ;)");
				sendRequests();

			} catch (IOException e1) {

				try {
					reconnections++;
					connectionError = true;
					//System.out.println("nao funcionei!!");
					Thread.sleep(2000);	
					s = null;

				} catch (InterruptedException e2) {e1.printStackTrace();}
			}
		}


	}



	/*********************************************************************************************/
	/****************************     MENUS     **************************************************/
	/*********************************************************************************************/



	/**
	 * Main Menu to login & register
	 */
	private static int mainMenu() {
		int option;
		try {
			do {
				System.out.print("\033[H\033[2J");
				System.out.println("###   Meeting Center   ###");
				System.out.println("\n.:: --- MAIN MENU --- ::.");
				option = askForNumber("1 - Login\n2 - Register\n0 - Quit\n", 0, 2);
				switch (option) {
				case 1:
					loginMenu();
					break;
				case 2:
					registerMenu();
					break;
				case 0:
					option = 0;
					break;
				default:
					break;
				}
			} while (option != 0);

			return 0;

		} catch (Exception e) { //ISTO DEPOIS é IOEXCEPTION

			System.out.println("Something went wrong: MAIN (): " + e);

		}

		return 0;
	}

	/**
	 * Login Menu
	 */
	private static void loginMenu() {
		Scanner s1 = new Scanner(System.in);
		String user = "", pass = "", data;
		int option;

		try {
			do {
				System.out.print("\033[H\033[2J");
				System.out.println(".:: --- LOGIN --- ::.");

				while (true) {
					System.out.println("> Insert your username:");
					user = s1.nextLine();
					System.out.println("> Insert your password:");
					pass = s1.nextLine();
					if (user.length() < 3) {
						System.out.println("Error: Username too short!");
					} else {
						break;
					}

					if (pass.length() < 6) {
						System.out.println("Error: Password too short! (min 6 chars)");
					} else {
						break;
					}
				}

				option = 0;

				sendToServer("LOG#" + user + "#" + pass);

				data = receiveReply(); // FUNC DO SERVER

				if (!data.equals("err")) {
					client_name = user;
					client_id = Integer.parseInt(data);
					clientMenu();
					break;
				} else {
					option = askForNumber("\n1 - Try again\n0 - Return to menu\n", 0, 1);
				}

			} while (option != 0);

		} catch (Exception e) {
			//System.out.println("Something went wrong: LoginMenu: "+e);
			handleIOException("LOG#" + user + "#" + pass);
		}

	}

	/**
	 * Register Menu
	 */
	private static void registerMenu() {
		String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
		boolean emailCheck;
		Scanner s1 = new Scanner(System.in);

		String mail, name, username, password, data;
		int option;

		try {

			do {
				System.out.print("\033[H\033[2J");
				System.out.println(".:: --- REGISTER --- ::.");
				while (true) {
					System.out.println(">> Full name:");

					name = s1.nextLine();
					if (isAlpha(name) == false || name.length() < 3) {
						System.out.println("Error: Invalid name");
					} else {
						break;
					}
				}

				while (true) {
					System.out.println(">> E-mail:");
					mail = s1.nextLine();
					emailCheck = mail.matches(EMAIL_REGEX);
					if (!emailCheck) {
						System.out.println("ERROR: Invalid e-mail. Please insert a valid one!");
					} else {
						break;
					}
				}

				username = askForText(">> Insert the desired username:");
				password = askForText(">> Insert the desired password:");

				option = 0;

				sendToServer("REG#" + name + "#" + mail + "#" + username + "#" + password);

				data = receiveReply();

				if (!data.equals("err")) {
					askForNumber("\nUser registered with success!\nPress 0 to return to menu: ", 0, 0);
					break;
				} else {
					option = askForNumber("\n1 - Try again\n0 - Return to menu\n", 0, 1);
				}

			} while (option != 0);

		} catch (Exception e) {
			System.out.println("Something went wrong: RegisterMenu: " + e);
		}

	}

	/**
	 * Client Menu
	 */
	private static void clientMenu() {
		int option;
		String notif;
		try {
			do {
				System.out.print("\033[H\033[2J");
				System.out.println(".:: --- " + client_name.toUpperCase() + "'S MENU --- ::.");
				System.out.println("1 - Schedule a meeting\n2 - Enter in a current meeting\n3 - Check all my meetings\n4 - Check my actions\n5 - Groups\n6 - List online users\n0 - Logout\n");
				sendToServer("BOU#"+client_id+"#"+client_name);
				notif = receiveReply();
				if (!notif.equals("ok"))
					System.out.println(notif);

				option = askForNumber("", 0, 6);

				switch (option) {
				case 1:
					scheduleMeetings();
					break;
				case 2:
					currentMeetings();
					break;
				case 3:
					checkMeetings();
					break;
				case 4:
					checkActions();
					break;
				case 5:
					groupsMenu();
					break;
				case 6:
					listOnlineUsers();
					break;
				case 0:
					break;
				default:
					System.out.println("Invalid option.");
					break;
				}
			} while (option != 0);

			sendToServer("BOF#"+client_name);
			receiveReply();
		} catch (Exception e) {
			System.out.println("Something went wrong: clientMenu() " + e);
		}
	}

	private static void groupsMenu()
	{
		int option; 
		try {
			do {
				System.out.print("\033[H\033[2J");
				System.out.println(".:: --- " + client_name.toUpperCase() + "'S GROUPS --- ::.");
				System.out.println("1 - Create a group\n2 - Check all my groups\n3 - Accept/Decline Groups\n0 - Return\n");
				option = askForNumber("", 0, 3);

				switch (option) {
				case 1:
					createGroup();
					break;
				case 2:
					checkGroups();
					break;
				case 3:
					acceptDeclineGroups();
				case 0:
					break;
				default:
					System.out.println("Invalid option.");
					break;
				}

			} while (option != 0);		

		} catch (Exception e) {
			System.out.println("Something went wrong: groupsMenu() " + e);
		}

	}

	private static void createGroup()
	{
		String name, confirm;
		int option, groupID;

		try {
			do {
				System.out.print("\033[H\033[2J");
				System.out.println("<<< Create a Group >>>");
				name = askForText("> Insert name:");

				if ((option = askForNumber("1 - Confirm\n2 - Edit name\n0 - Return\n", 0, 2)) == 1) {
					sendToServer("CGR#" + name + "#" + client_id+"#"+client_name);
					confirm = receiveReply();

					if (confirm.equals("err")) {
						if ((option = askForNumber("1 - Try again\n0 - Return\n", 0, 1)) == 1) {
							option = 2;
						}
					} else {
						groupID = Integer.parseInt(confirm);

						System.out.println("\nDo you wish to invite users?");
						if (askForNumber("1 - Yes\n2 - No\n", 1, 2) == 1) {
							addUsers(groupID,1);
						}
					}
				}
			}while(option==2);
		} catch (Exception e) {
			System.out.println("Something went wrong: createGroup() " + e);
		}
	}

	private static void checkGroups()
	{
		String groups;
		int i,groupID=0;
		ArrayList<Integer> ids = new ArrayList<Integer>();

		try {

			do {
				System.out.print("\033[H\033[2J");
				System.out.println("<<< "+client_name+"'s Groups >>>");

				sendToServer("PUG#" + client_id); //print user groups
				groups = receiveReply();

				if (groups.charAt(0) != '0') {

					i=groups.indexOf('\n');

					ids = retrieveIDs(groups);

					System.out.println("\n[Status] ID -> Groups ************************\n");
					System.out.println(groups.substring(i + 1));
					System.out.println("************************************");

					System.out.println("\n.:Select its respective ID to detailed view or press 0 to return:.");

					if ((groupID = getID(ids,"group"))!=0)
						detailedGroup(groupID);

				} else {
					askForNumber("\nThere are no groups. Press 0 to return: ", 0, 0);
					break;
				}

			}while(groupID!=0);
		} catch (Exception e1) {
			System.out.println("Something went wrong: checkGroups()" + e1);
		}
	}

	private static void detailedGroup(int groupID)
	{
		try{
			System.out.print("\033[H\033[2J");
			System.out.println("<<< " + client_name + "'s Group >>>");

			sendToServer("PDG#" + groupID);
			System.out.println(receiveReply());

			System.out.println("\nDo you wish to invite users?");

			if (askForNumber("1 - Yes\n2 - No\n", 1, 2) == 1)
				addUsers(groupID,1);


		} catch (Exception e) {
			System.out.println("Something went wrong:  detailedGroup(): " + e);
		}		
	}

	private static void acceptDeclineGroups()
	{
		String groups;
		int i, groupID, option;
		ArrayList <Integer> ids = new ArrayList<Integer>();

		try {
			System.out.print("\033[H\033[2J");
			System.out.println("<<< " + client_name + "'s Groups >>>");
			sendToServer("PUG#" + client_id);
			groups = receiveReply();

			if (groups.charAt(0) != '0') {

				i = groups.indexOf('\n');
				ids = retrieveIDs(groups);

				System.out.println("\n[Status] ID -> Groups ************************\n");
				System.out.println(groups.substring(i + 1));
				System.out.println("************************************");
				System.out.println(".:Select its respective ID to accept/decline or press 0 to return:.");

				while (true) {
					groupID = getID(ids,"group");

					if (groupID != 0){
						option = askForNumber("Do you wish to accept or decline?\n1 - Accept\n2 - Decline\n", 1, 2);
						sendToServer("ADG#" + groupID + "#" + client_id + "#" + option);
						System.out.println(receiveReply());
					}
					else
						break;
				}

			} else {
				askForNumber("\nThere are no groups. Press 0 to return: ", 0, 0);
			}
		} catch (Exception e) {
			System.out.println("Something went wrong:  acceptDeclineGroups(): " + e);
		}		
	}

	private static void listOnlineUsers() {
		System.out.print("\033[H\033[2J");
		System.out.println("<<< Online Users >>>\n");
		sendToServer("LOU#");
		System.out.println(receiveReply());
		askForNumber("Press 0 to return: ", 0, 0);
	}

	/**
	 * Schedule a Meeting
	 */
	private static void scheduleMeetings() {
		String title, outcome, location, confirm;
		String startDate, endDate, totalGroups;
		int meetID, option,i, groupID;
		ArrayList<Integer> ids = new ArrayList<Integer>();

		try {

			do {
				System.out.print("\033[H\033[2J");
				System.out.println("<<< Schedule a Meeting >>>");
				title = askForText("> Insert title:");
				outcome = askForText("> Insert outcome:");
				startDate = askForDate("","> Insert start time (dd/MM/aaaa HH:mm):");
				endDate = askForDate(startDate,"> Insert end time (dd/MM/aaaa HH:mm):");

				location = askForText("> Insert location:");

				if ((option = askForNumber("\n1 - Confirm\n2 - Edit fields\n0 - Return\n", 0, 2)) == 1) {
					sendToServer("AME#" + title + "#" + outcome + "#" + startDate + "#" + endDate + "#" + location + "#" + client_id+"#"+client_name);
					confirm = receiveReply();

					if (confirm.equals("err")) {
						if ((option = askForNumber("\n1 - Try again\n0 - Return\n", 0, 1)) == 1) {
							option = 2;
						}
					} else {
						meetID = Integer.parseInt(confirm);

						System.out.println("Do you wish to add agenda items?");
						if (askForNumber("1 - Yes\n2 - No\n", 1, 2) == 1)           	
							addAgendaItems(meetID);

						System.out.println("\nDo you wish to invite groups?");
						if (askForNumber("1 - Yes\n2 - No\n", 1, 2) == 1) {

							sendToServer("PAG#"); //PAG = Print All Groups	
							totalGroups = receiveReply(); //mostra todos 

							if (totalGroups.charAt(0) != '0') {

								i=totalGroups.indexOf('\n');

								ids = retrieveIDs(totalGroups);

								System.out.println("\nID -> GROUPS ***********************\n");
								System.out.println(totalGroups.substring(i + 1));
								System.out.println("************************************");

								System.out.println("Keep entering their respective ID and press 0 when finished:");

								while (true) {
									groupID = getID(ids,"group");

									if (groupID!=0){
										sendToServer("AGM#"+ meetID + "#" + groupID);
										System.out.println(receiveReply()); //invited
									}

									else
										break;
								}

							} else {
								askForNumber("\nThere are no groups. Press 0 to return: ", 0, 0);
								break;
							}
						}

						System.out.println("\nDo you wish to invite users?");
						if (askForNumber("1 - Yes\n2 - No\n", 1, 2) == 1) {
							addUsers(meetID,0);
						}
					}
				}

			} while (option == 2);

		} catch (Exception e) {
			System.out.println("Something went wrong: ScheduleMeetings() " + e);
		}
	}

	/**
	 * Client's Current Meetings
	 */
	private static void currentMeetings() 
	{
		String meetings;
		int i,meetID=0;
		ArrayList<Integer> ids = new ArrayList<Integer>();

		try {

			do {
				System.out.print("\033[H\033[2J");
				System.out.println("<<< "+client_name+"'s Current Meeting >>>");

				sendToServer("PCM#" + client_id);
				meetings = receiveReply();

				if (meetings.charAt(0) != '0') {

					i=meetings.indexOf('\n');

					ids = retrieveIDs(meetings);

					System.out.println("\nID -> Meetings ************************\n");
					System.out.println(meetings.substring(i + 1));
					System.out.println("************************************");

					System.out.println("\n.:Select its respective ID to detailed view or press 0 to return:.");

					if ((meetID = getID(ids,"meeting"))!=0)
						enterMeeting(meetID);

				} else {
					askForNumber("\nThere are no current meetings. Press 0 to return: ", 0, 0);
					break;
				}

			}while(meetID!=0);
		} catch (Exception e1) {
			System.out.println("Something went wrong: CurrentMeetings()" + e1);
		}
	}

	/**
	 * Decisions Menu
	 */
	private static void decisionsMenu(int itemID) {
		Scanner s1 = new Scanner(System.in);
		String line;

		try{
			System.out.print("\033[H\033[2J");
			System.out.println("<<< Add Decisions >>>");
			sendToServer("PID#" + itemID);
			System.out.println(receiveReply());
			System.out.println("\nDo you wish to add a decision to this item?");

			if (askForNumber("1 - Yes\n2 - No\n", 1, 2) == 1) {
				do {
					System.out.print("\033[H\033[2J");
					System.out.println("<<< Add Decisions >>>");
					sendToServer("PID#" + itemID);
					System.out.println(receiveReply());

					line = askForText("> Insert decision:");
					sendToServer("AID#" + itemID + "#" + "["+client_name+"] "+line);
					System.out.println(receiveReply());

				} while (askForNumber("Do you wish to add more decisions?\n1 - Yes\n2 - No\n", 1, 2) == 1);
			}

		} catch (Exception e1) {
			System.out.println("Something went wrong: decisionsMenu()" + e1);
		}
	}

	/**
	 * Actions Menu
	 */
	private static void actionsMenu(int itemID) {
		Scanner s1 = new Scanner(System.in);
		String line;
		String totalUsers, totalGroups;
		int i, id;
		ArrayList <Integer> ids = new ArrayList<Integer>();

		try{
			sendToServer("PIA#" + itemID);
			System.out.print("\033[H\033[2J");
			System.out.println("<<< Add Actions >>>");
			System.out.println(receiveReply());

			System.out.println("\nDo you wish to assign a task to a group?");
			if (askForNumber("1 - Yes\n2 - No\n", 1, 2) == 1) {
				do {
					System.out.print("\033[H\033[2J");
					System.out.println("<<< Add Actions >>>");
					sendToServer("PAG#"); //PAG = Print All Groups	
					totalGroups = receiveReply(); //mostra todos 


					if (totalGroups.charAt(0) != '0') {

						i=totalGroups.indexOf('\n');

						ids = retrieveIDs(totalGroups);


						System.out.println("\nID -> GROUPS ***********************\n");
						System.out.println(totalGroups.substring(i + 1));
						System.out.println("************************************");

						if ((id = getID(ids,"group")) != 0){
							line = askForText("> Insert action:");
							sendToServer("AAG#" + itemID + "#" + id + "#" + line);
							System.out.println(receiveReply());
						}


					} else {
						askForNumber("\nThere are no groups. Press 0 to return: ", 0, 0);
						break;
					}

				} while (askForNumber("Do you wish to assign more tasks to a group?\n1 - Yes\n2 - No\n", 1, 2) == 1);

			}

			System.out.println("\nDo you wish to assign a task to a user?");
			if (askForNumber("1 - Yes\n2 - No\n", 1, 2) == 1) {
				do {
					System.out.print("\033[H\033[2J");
					System.out.println("<<< Add Actions >>>");
					sendToServer("PAU#" + client_id); //PAU = Print All Users		
					totalUsers = receiveReply(); //mostra todos os user excepto este client


					if (totalUsers.charAt(0) != '0') {

						i=totalUsers.indexOf('\n');

						ids = retrieveIDs(totalUsers);


						System.out.println("\nID -> USERS ************************\n");
						System.out.println(totalUsers.substring(i + 1));
						System.out.println("************************************");

						if ((id = getID(ids,"user")) != 0){
							line = askForText("> Insert action:");
							sendToServer("AAU#" + itemID + "#" + id + "#" + line);
							System.out.println(receiveReply());
						}


					} else {
						askForNumber("There are no more users. Press 0 to return: ", 0, 0);
						break;
					}

				} while (askForNumber("Do you wish to assign more tasks to a user?\n1 - Yes\n2 - No\n", 1, 2) == 1);

			}


		} catch (Exception e1) {
			System.out.println("Something went wrong: actionsMenu()" + e1);
		}

	}

	/**
	 * Check client's meetings
	 */
	private static void checkMeetings() {
		try {

			int option;
			do {
				option = 0;
				System.out.print("\033[H\033[2J");
				System.out.println(".:: --- " + client_name.toUpperCase() + "'S MEETINGS --- ::.");
				option = askForNumber("1 - See all meetings\n2 - Accept/Decline\n0 - Return\n\n", 0, 2);
				switch (option) {
				case 1:
					showMeetings();
					break;
				case 2:
					acceptDeclineMeetings();
				case 0:
					break;
				default:
					System.out.println("Invalid option.");
					break;
				}
			} while (option != 0);

		} catch (Exception e) {
			System.out.println("Something went wrong:  checkMeetings(): " + e);
		}

	}

	/**
	 * Meetings Overview
	 */
	private static void showMeetings() {

		Scanner s1 = new Scanner(System.in);
		String meetings;
		int i, meetID;
		ArrayList<Integer> ids = new ArrayList <Integer>();

		try {
			do {
				System.out.print("\033[H\033[2J");
				System.out.println("<<< " + client_name + "'s Meetings >>>");

				sendToServer("PUM#" + client_id);
				meetings = receiveReply();

				if (meetings.charAt(0) != '0') {

					i = meetings.indexOf('\n');

					ids = retrieveIDs(meetings);

					System.out.println("\n[Status] ID -> Meetings ************************\n");
					System.out.println(meetings.substring(i + 1));
					System.out.println("************************************");
					System.out.println("\n.:Select its respective ID to detailed view or press 0 to return:.");


					if ((meetID = getID(ids,"meeting"))!=0)
						detailedMeeting(meetID);


				} else {
					askForNumber("\nThere are no meetings. Press 0 to return: ", 0, 0);
					break;
				}

			}while (meetID!=0);

		} catch (Exception e) {
			System.out.println("Something went wrong:  showMeetings(): " + e);
		}
	}

	/**
	 * Check client's actions
	 */	
	private static void checkActions() {
		String actions;
		int i, actionID;
		ArrayList <Integer> ids = new ArrayList <Integer>();

		try {
			System.out.print("\033[H\033[2J");
			System.out.println("<<< " + client_name + "'s Actions >>>");
			sendToServer("PUA#" + client_id);
			actions = receiveReply();

			if (actions.charAt(0) != '0') {

				i = actions.indexOf('\n');
				ids = retrieveIDs(actions);

				System.out.println("\nID -> Actions ************************\n");
				System.out.println(actions.substring(i + 1));
				System.out.println("************************************");
				System.out.println("\n.:Select its respective ID to mark as done or press 0 to return:.");

				while (true) {
					if ((actionID = getID(ids,"action")) == 0) {
						break;
					}

					else{
						sendToServer("MAD#" + actionID);
						System.out.println(receiveReply());
					}

				}
			} else {
				askForNumber("\nThere are no actions. Press 0 to return: ", 0, 0);
			}
		} catch (Exception e) {
			System.out.println("Something went wrong:  checkActions(): " + e);
		}
	}

	/**
	 * Accept/Decline Meetings
	 */	
	private static void acceptDeclineMeetings() {
		String meetings;
		int i, meetID, option;
		ArrayList <Integer> ids = new ArrayList<Integer>();

		try {
			System.out.print("\033[H\033[2J");
			System.out.println("<<< " + client_name + "'s Upcoming Meetings >>>");
			sendToServer("PUU#" + client_id);
			meetings = receiveReply();

			if (meetings.charAt(0) != '0') {

				i = meetings.indexOf('\n');
				ids = retrieveIDs(meetings);

				System.out.println("\n[Status] ID -> Meetings ************************\n");
				System.out.println(meetings.substring(i + 1));
				System.out.println("************************************");
				System.out.println("\n.:Select its respective ID to accept/decline or press 0 to return:.");

				while (true) {
					meetID = getID(ids,"meeting");

					if (meetID != 0){
						option = askForNumber("Do you wish to accept or decline?\n1 - Accept\n2 - Decline\n", 1, 2);
						sendToServer("ADM#" + meetID + "#" + client_id + "#" + option);
						System.out.println(receiveReply());
					}
					else
						break;
				}

			} else {
				askForNumber("\nThere are no meetings. Press 0 to return: ", 0, 0);
			}
		} catch (Exception e) {
			System.out.println("Something went wrong:  acceptDeclineMeetings(): " + e);
		}
	}

	/**
	 * Add agenda items to a meeting
	 */	
	private static void addAgendaItems(int meetID)
	{
		String input;

		try {

			System.out.println("Keep entering them and press 0 when finished:");
			while (true) {

				if ((input = askForText("> Insert item name:")).equals("0")) {
					break;
				}

				sendToServer("AMI#" + meetID + "#" + input + "#" + client_name);
				System.out.println(receiveReply());
			}

		} catch (Exception e) {
			System.out.println("Something went wrong:  addAgendaItems(): " + e);
		}
	}

	/**
	 * Invite users to a meeting or group
	 */	
	private static void addUsers(int id, int meetGroup)
	{
		String totalUsers;
		int i,userID;
		ArrayList <Integer> ids = new ArrayList<Integer>();

		try {
			sendToServer("PAU#" + client_id); 
			totalUsers = receiveReply(); 

			if (totalUsers.charAt(0) != '0') {

				i = totalUsers.indexOf('\n');
				ids = retrieveIDs(totalUsers);

				System.out.println("\nID -> USERS ************************\n");
				System.out.println(totalUsers.substring(i + 1));
				System.out.println("************************************");

				System.out.println("Keep entering their respective ID and press 0 when finished:");
				while (true) {
					userID = getID(ids,"user");

					if (userID!=0){
						if (meetGroup == 0)
							sendToServer("AUM#" + id + "#" + userID);
						else
							sendToServer("AUG#"+ id + "#" + userID);

						System.out.println(receiveReply()); //invited
					}

					else
						break;
				}
			} else {
				askForNumber("There are no users available.. Press 0 to return:\n", 0, 0);
			}
		} catch (Exception e) {
			System.out.println("Something went wrong:  addUsers(): " + e);
		}
	}

	/**
	 * Entering a meeting
	 */	
	private static void enterMeeting(int meetID)
	{
		String items;
		int i, itemID, option;
		ArrayList<Integer> ids = new ArrayList<Integer>();

		try {
			do {
				System.out.print("\033[H\033[2J");
				System.out.println("<<< You are now in a Meeting >>>");

				sendToServer("PUC#" + meetID);
				System.out.println(receiveReply());

				sendToServer("PMI#" + meetID);
				items = receiveReply();

				i= items.indexOf('\n');
				ids = retrieveIDs(items);

				System.out.println("ID -> Items ************************\n");
				System.out.println(items.substring(i + 1));
				System.out.println("************************************");
				System.out.println("\n.:Select its respective ID or press 0 to return:.");

				itemID = getID(ids,"item");

				if (itemID!=0)
				{
					do {
						System.out.print("\033[H\033[2J");
						System.out.println("<<< Item Menu >>>");
						option = askForNumber("1 - Check/add key decisions\n2 - Check/add actions\n3 - Enter chat\n0 - Return\n", 0, 3);

						switch (option) {
						case 1: 
							decisionsMenu(itemID);
							break;
						case 2: 
							actionsMenu(itemID);
							break;
						case 3: 
							enterChat(meetID,itemID);
							break;

						case 0:
							break;
						default:
							System.out.println("Invalid option.");
							break;
						}

					} while (option != 0);
				}

			}while (itemID!=0);

		} catch (Exception e) {
			System.out.println("Something went wrong:  enterMeeting(): " + e);
		}
	}

	/**
	 * Entering a chat
	 */	
	private static void enterChat(int meetID, int itemID)
	{
		String line;
		Scanner s1 = new Scanner(System.in);

		try {
			while (true) {
				sendToServer("PIC#" + itemID);
				System.out.print("\033[H\033[2J");
				System.out.println(receiveReply());

				System.out.println("To quit, write '/quit'");

				line = askForText("");
				if (!line.equals("/quit")) {
					sendToServer("S2C#" + meetID + "#" + itemID + "#" + client_name+": " +line);
					System.out.println(receiveReply());
				} else {
					break;
				}
			}

		} catch (Exception e) {
			System.out.println("Something went wrong:  enterChat(): " + e);
		}
	}

	/**
	 * Meeting details
	 */	
	private static void detailedMeeting(int meetID)
	{
		int i, crono, option=0;

		try{
			do{
				System.out.print("\033[H\033[2J");
				System.out.println("<<< " + client_name + "'s Meeting >>>");

				sendToServer("PUC#" + meetID);
				System.out.println(receiveReply());

				sendToServer("GMC#" + meetID);
				crono = Integer.parseInt(receiveReply());

				switch (crono){
				case -1:
					pastMeeting(meetID);break;
				case 0:
					currentMeeting(meetID);break;
				case 1:
					option = upcomingMeeting(meetID);
					break;
				default:
					System.out.println("Invalid option.");
					break;
				}
			}while(option!=0);

		} catch (Exception e) {
			System.out.println("Something went wrong:  detailedMeeting(): " + e);
		}
	}

	/**
	 * Past Meeting options
	 */	
	private static void pastMeeting(int meetID){
		int itemID, option, status, i;
		String items;	
		ArrayList<Integer> ids = new ArrayList<Integer>();

		try{
			sendToServer("PMI#" + meetID);
			items = receiveReply();

			i = items.indexOf('\n');

			ids = retrieveIDs(items);

			System.out.println("ID -> Items ************************\n");
			System.out.println(items.substring(i + 1));
			System.out.println("************************************");
			System.out.println("\n.:Select its respective ID to detailed view or press 0 to return:.");

			itemID = getID(ids,"item");

			if (itemID != 0) {

				sendToServer("GUS#" + meetID + "#" + client_id);
				status = Integer.parseInt(receiveReply());

				do {
					System.out.print("\033[H\033[2J");
					System.out.println("<<< Meeting Item >>>");
					System.out.println("\nSelect one of the following:");

					if (status == 2) 
						option = askForNumber("1 - Read chat\n2 - Read/Add Key Decisions\n3 - Read/Add Actions\n0 - Return\n", 0, 3);
					else 
						option = askForNumber("1 - Read chat\n2 - Read Key Decisions\n3 - Read Actions\n0 - Return\n", 0, 3);


					System.out.print("\033[H\033[2J");

					switch (option) {
					case 1: 
						System.out.println("<<< " + client_name + "'s Meeting >>>");
						sendToServer("PIC#" + itemID);
						System.out.println(receiveReply());

						askForNumber("Press 0 to return: ", 0, 0);

						break;

					case 2: 
						sendToServer("PID#" + itemID);
						System.out.println(receiveReply());

						if (status == 2) {
							if (askForNumber("1 - Add Key Decisions\n0 - Return\n", 0, 1) == 1)
								decisionsMenu(itemID);

						} else {
							askForNumber("Press 0 to return: ", 0, 0);
						}
						break;

					case 3: 
						sendToServer("PIA#" + itemID);
						System.out.println(receiveReply());

						if (status == 2) {
							if (askForNumber("1 - Add Actions\n0 - Return\n", 0, 1) == 1) 
								actionsMenu(itemID);

						} else {
							askForNumber("Press 0 to return: ", 0, 0);
						}
						break;
					case 0:
						break;

					default:
						System.out.println("Invalid option.");
						break;
					}
				} while (option != 0);
			}

		} catch (Exception e) {
			System.out.println("Something went wrong:  pastMeeting(): " + e);
		}
	}

	/**
	 * Current Meeting options
	 */	
	private static void currentMeeting(int meetID)
	{
		int itemID,option,i;
		String items;	
		ArrayList<Integer> ids = new ArrayList<Integer>();

		try{
			sendToServer("PMI#" + meetID);
			items = receiveReply();

			i = items.indexOf('\n');

			ids = retrieveIDs(items);

			System.out.println("ID -> Items ************************\n");
			System.out.println(items.substring(i + 1));
			System.out.println("************************************");
			System.out.println("\n.:Select its respective ID to detailed view or press 0 to return:.");


			itemID = getID(ids,"item");

			if (itemID != 0) {

				do {
					System.out.print("\033[H\033[2J");
					System.out.println("<<< Meeting Item >>>");
					System.out.println("\nSelect one of the following:");

					option = askForNumber("1 - Read chat\n2 - Read Key Decisions\n3 - Read Actions\n0 - Return\n", 0, 3);

					System.out.print("\033[H\033[2J");

					switch (option) {
					case 1: 
						sendToServer("PIC#" + itemID);
						System.out.println(receiveReply());
						askForNumber("Press 0 to return: ", 0, 0);
						break;
					case 2: 
						sendToServer("PID#" + itemID);
						System.out.println(receiveReply());
						askForNumber("Press 0 to return: ", 0, 0);
						break;

					case 3: 
						sendToServer("PIA#" + itemID);
						System.out.println(receiveReply());
						askForNumber("Press 0 to return: ", 0, 0);
						break;
					case 0:
						break;

					default:
						System.out.println("Invalid option");
						break;
					}
				} while (option != 0);
			}
		} catch (Exception e) {
			System.out.println("Something went wrong:  currentMeeting(): " + e);
		}
	}

	/**
	 * Upcoming Meeting options
	 */	
	private static int upcomingMeeting(int meetID){
		int option, itemID, i;
		String line,items;
		ArrayList<Integer> ids = new ArrayList<Integer>();
		int count = 0;

		try{
			do {
				count++;
				sendToServer("PMI#" + meetID);
				items = receiveReply();

				i = items.indexOf('\n');

				ids = retrieveIDs(items);

				System.out.println("ID -> Items ************************\n");
				System.out.println(items.substring(i + 1));
				System.out.println("************************************");

				System.out.println("\nSelect one of the following:");
				option = askForNumber("1 - Add items\n2 - Modify Item\n3 - Remove Item\n0 - Return\n", 0, 3);

				switch (option) {
				case 1: 
					addAgendaItems(meetID);
					break;
				case 2: 
					System.out.println("\n.:Select its respective ID or press 0 to return:.");

					if ((itemID=getID(ids,"item"))!=0){
						line = askForText("> Insert new name:");

						sendToServer("MMI#" + itemID + "#" + line);
						System.out.println(receiveReply());
						askForNumber("Press 0 to return: ", 0, 0);
					}


					break;

				case 3: 
					System.out.println("\n.:Select its respective ID or press 0 to return:.");
					if ((itemID=getID(ids,"item"))!=0){
						sendToServer("RMI#" + itemID);
						System.out.println(receiveReply());
						askForNumber("Press 0 to return: ", 0, 0);
					}
					break;
				case 0:
					break;

				default:
					System.out.println("Invalid option.");
					break;
				}

				System.out.print("\033[H\033[2J");
				System.out.println("<<< " + client_name + "'s Meeting >>>");

			} while (option != 0);

		} catch (Exception e) {
			System.out.println("Something went wrong:  upcomingMeeting(): " + e);
		}

		if (count>1)
			return 1;	

		return 0; //quer sair da meeting

	}

	private static String receiveReply() {
		String reply;

		try {
			reply = in.readUTF();
		} catch (IOException e) {
			handleIOException();
			return null;
		}
		String[] split = reply.split("#");

		if (split[0].equals("ok")) {
			if (split.length == 1) {
				return "ok";
			} else {
				if (split.length == 3) {
					System.out.println(split[1]);
					return split[2];
				} else {
					return split[1];
				}
			}
		}

		//	Print all users		
		if (split[0].equals("print")) {
			return split[1];
		}

		if (split[0].equals("err")) {
			System.out.println(split[1]);
			return split[0];
		}

		return reply;
	}

	/*********************************************************************************************/
	/******************************** AUXILIARY METHODS ******************************************/
	/*********************************************************************************************/

	private static String askForText(String question) {
		Scanner s1 = new Scanner(System.in);
		String input;
		System.out.println(question);

		while (true) {
			input = s1.nextLine();
			if (input.contains("#") || (input.length() < 3 && !input.equals("0"))) {
				System.out.println("Error: Cannot contain # or have less than 3 characters.");
			} else {
				break;
			}
		}
		return input;
	}

	private static int askForNumber(String question, int min, int max) {
		Scanner s1 = new Scanner(System.in);
		String input;
		int output = -1;
		System.out.print(question);
		do {
			input = s1.nextLine();
			if (input.equals("\n") || input.equals("")) {
				System.out.println("Error: Too short.");
			} else if (checkNumber(input) == false) {
				System.out.println("Error: Not a number.");
			} else {
				output = Integer.parseInt(input);
				if (output < min || output > max) {
					System.out.println("Error: Invalid option.");
				}
			}
		} while (output < min || output > max);
		return output;
	}

	private static boolean checkNumber(String num) {
		try {
			Integer.parseInt(num);
			return true;
		} catch (NumberFormatException e) {
			//System.out.println("Error: Could not convert string to integer...");
			return false;
		}
	}

	private static String askForDate(String date, String question) throws ParseException {
		Scanner s1 = new Scanner(System.in);
		String input;
		boolean check;

		do {
			System.out.println(question);
			input = s1.nextLine();

			if (date.isEmpty())
				check = validateDateFormat(input,"");
			else
				check = validateDateFormat(date,input);

		} while (check);

		return input;
	}

	private static boolean validateDateFormat(String date1, String date2) { 
		String[] formatStrings = {"dd/MM/yyyy HH:mm"};
		boolean isInvalidFormat = false;
		Date dateObj, dateObj2;
		Date now = new Date();
		for (String formatString : formatStrings) {
			try {
				SimpleDateFormat sdf = (SimpleDateFormat) DateFormat.getDateInstance();
				sdf.applyPattern(formatString);
				sdf.setLenient(false);
				dateObj = sdf.parse(date1);
				if (date1.equals(sdf.format(dateObj))) {
					isInvalidFormat = false;
					if (dateObj.before(now)) {
						System.out.println("Error: Invalid date format. The date has already passed!\n");
						isInvalidFormat = true;
					}

					if (!date2.isEmpty())
					{
						dateObj2 = sdf.parse(date2);
						if (date2.equals(sdf.format(dateObj2))) {
							isInvalidFormat = false;

							if (dateObj2.before(dateObj)) {
								System.out.println("Error: End time cannot be before start time!\n");
								isInvalidFormat = true;
							}
						}
					}
					break;
				}
			} catch (ParseException e) {
				isInvalidFormat = true;
				System.out.println("Error: Invalid date format. Please insert a valid date!\n");
			}
		}

		return isInvalidFormat;
	}

	private static ArrayList<Integer> retrieveIDs (String data)
	{
		int i,j,k;
		String n;
		String[] token;
		ArrayList <Integer> temp = new ArrayList<Integer>();

		token = data.split("\n");

		for (i = 1; i < token.length; i++) {
			j = token[i].indexOf(' ');
			n = token[i].substring(0, j);
			if (checkNumber(n))
				temp.add(Integer.parseInt(n));
			else
			{
				k = token[i].indexOf(' ', j+1);
				n = token[i].substring(j+1, k);
				if (checkNumber(n))
					temp.add(Integer.parseInt(n));
			}
		}

		return temp;
	}

	private static int getID (ArrayList <Integer> ids, String word)
	{
		int id;

		while (true) {
			if ((id = askForNumber("> Insert "+word+" ID: ", 0, 100)) == 0) 
				break;

			if (!ids.contains(id))
				System.out.println("Error: Invalid ID.");
			else
				break;
		}

		return id;
	}

	private static final Pattern LETTERS = Pattern.compile("\\w.*");

	private static boolean isAlpha(String text) {
		return LETTERS.matcher(text).matches();
	}

}

/*//= Thread para tratar da conecção UDP com o outro servidor (ping)
 class Reader extends Thread {
 private  Socket s;
 private DataInputStream in;
 private DataOutputStream out;
 Reader(Socket s1, DataInputStream in, DataOutputStream out){
 this.s = s;
 this.in = in;
 this.out = out;
 }

 public void run(){


 while(true){

 String reply ="";


 try {
 synchronized(s){
 reply = in.readUTF(); //Bloqueante
 }

 System.out.println("Li: "+reply);

 } catch (IOException e) {

 System.out.println("Erro no IO da thread");
 }
 String[] split = reply.split("#");


 /*if (split[0].equals("ok")){
 if(split.length==1)

 //return split[0];
 else
 {
 if (split.length==3)
 //	return split[2];

 else 
 //	return split[1];
 }
 }*/
//	Print all users		
//			if(split[0].equals("print")){
//				System.out.println(split[1]);
//			}
//			
//			else if (split[0].equals("err")){
//				System.out.println(split[1]);
//				//return split[0];
//			}
//			else{
//				System.out.println("nhe");
//			}
//			
//			//return reply;
//			
//		
//	  
//}
//
//
//}
