import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Properties;

public class UDPping{
	
	private boolean isMain = false;
	private int lossPacket = 0;
	private static ArrayList<String> serverAddress = new ArrayList<String>();
	
	
	/*** INSTRUÇÕES ***/
	//O server inicia-se e tentar pingar para determinado host
	// LOOP:
		//Se não receber o "pong" torna-se o principal
		//Se receber o pong volta a enviar um ping
	
	
	
	protected UDPping(){
		loadProperties();
	}

	
	
	
	/**********************************************************************************************/
	/******************************** METODOS ****************************************************/
	/********************************************************************************************/
	
	
	public boolean getIsMain(){
		return isMain;
	}
	
	
	public static void loadProperties(){
    	
    	
  	  InputStream in = null;
  	    try {
  	      Properties properties = new Properties();
  	      in = new FileInputStream("app.properties");
  	      properties.load(in);
  	      
  	      serverAddress.add(properties.getProperty("server1.address"));
  	      serverAddress.add(properties.getProperty("server2.address"));
  	          
  	    }
  	    catch(IOException e) {
  	      e.printStackTrace();
  	    }
  	    finally {
  	      if(in != null) {
  	        try {
  	          in.close();
  	        }
  	        catch(IOException e) { }
  	      }
  	    }
		
	}
	
	
	
	
	
	public void tryToConnect(){
		
		
		DatagramSocket aSocket = null;
	
		
		try {
			
			
			String ping = "ping";
			String pong = "pong";
			
			byte [] m = ping.getBytes(); 
			
			byte [] po = pong.getBytes();
			
			aSocket = new DatagramSocket();    
			
			InetAddress aHost1 = InetAddress.getByName(serverAddress.get(0));
			InetAddress aHost2 = InetAddress.getByName(serverAddress.get(1));
			//System.out.println("IP 1 (FUNC UDP): "+aHost1.getHostAddress());
			//System.out.println("IP 2(FUNC UDP): "+aHost2.getHostAddress());
			
	

			int serverPort = 6789;		                                                
			DatagramPacket request1 = new DatagramPacket(po,po.length,aHost1,serverPort);
			DatagramPacket request2 = new DatagramPacket(po,po.length,aHost2,serverPort);
			aSocket.send(request1);
			aSocket.send(request2);	
			
			while(true){
				
				byte[] buffer = new byte[1024];
				DatagramPacket feedback = new DatagramPacket(buffer, buffer.length);	
				
			
				aSocket.setSoTimeout(350);
				aSocket.receive(feedback); //BLOQUEANTE
				//System.out.println(""+new String(feedback.getData(), 0, feedback.getLength()));
					
					
				
				DatagramPacket reply = new DatagramPacket(po,po.length, feedback.getAddress(), feedback.getPort());
				aSocket.send(reply);
				lossPacket = 0;
				
					
				
			} // while
			
		}catch(SocketTimeoutException e){
				
				//AQUI ASSUME O PAPEL DE SERVER PRINCIPAL
				
				lossPacket++;
				if(lossPacket == 20){
					isMain = true;
					aSocket.close();
					
				}
				else
					tryToConnect();
				
		}catch (SocketException e){System.out.println("Socket: " + e.getMessage());
		}catch (IOException e){System.out.println("IO: " + e.getMessage());
		}finally {if(aSocket != null) aSocket.close();}
	}
	
	
	
	
	

	/*public void isNowMainServer(){
		System.out.println("AGORA EU É QUE MANDO CARAGO!!");
		
		String ping = "ping";
		byte [] m = ping.getBytes(); 

		try {
			
			DatagramSocket newSocket = new DatagramSocket(6789);
		
			while(true){
				
				byte[] buffer = new byte[1024];
				DatagramPacket feedback = new DatagramPacket(buffer, buffer.length);	
				newSocket.receive(feedback); //BLOQUEANTE
				System.out.println(""+new String(feedback.getData(), 0, feedback.getLength()));
				
				Thread.sleep(200); //simulates network delays
	
				DatagramPacket reply = new DatagramPacket(m, 
						m.length, feedback.getAddress(), feedback.getPort());
				newSocket.send(reply);
				
			}
		
		}catch (SocketException e){
			tryToConnect();
		}
		
		catch (IOException e){System.out.println("IO: " + e.getMessage());
		} catch (InterruptedException e) {
			System.out.println("network delays problems :c");
			e.printStackTrace();
		}
		
	}*/
	
		
}
