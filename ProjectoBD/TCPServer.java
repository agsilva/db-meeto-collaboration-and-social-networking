import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

public class TCPServer {

	private static ServerSocket listenSocket = null;
	private static RMIServerInterface c;
	private static boolean startRMI = false;
	private static ArrayList<String> serverAddress = new ArrayList<String>();
	public static HashMap<Integer,String> connectedUsers= new HashMap<Integer,String>();

	/**
	 * * MAIN **
	 */
	public static void main(String[] args) {
		boolean isMain = false;
		loadProperties();
		UDPping udp = new UDPping(); // class where the functions who handle the UDP connections are.
		udp.tryToConnect();

		while (true) {
			isMain = udp.getIsMain();
			System.out.println("isMain: " + isMain);
			if (isMain) {
				try {
					if (listenSocket != null) {
						listenSocket.close();
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				Pings test = new Pings(); // creates a thread to handle the UDP connection 
				test.start();
				iniciaRMI();
				iniciaSockets();
				break;
			}

		}

	}

	/**
	 * * OUTROS METODOS **
	 */
	public static void loadProperties() {

		InputStream in = null;
		try {
			Properties properties = new Properties();
			in = new FileInputStream("app.properties");
			properties.load(in);

			serverAddress.add(properties.getProperty("server1.address"));
			//serverAddress.add(properties.getProperty("server2.address"));
			//serverAddress.add(properties.getProperty("server3.address"));

		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Something went wrong:  loadProperties(): " + e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
				}
			}
		}

	}

	public static synchronized void iniciaRMI() {

		try {
			/*  System.getProperties().put("java.security.policy", "/home/nocas/Área de Trabalho/SD/DEMO_3_RMI/RMI/hello_callback/security.policy"); 
             System.setSecurityManager(new RMISecurityManager()); */

			//Por agora vou deixar a segurança em stand by
			//Inicia interface RMI 
			String name = "rmi://" + serverAddress.get(0) + ":" + 1099 + "/RMIserver1";
			System.out.println("Address: " + serverAddress.get(0));
			c = (RMIServerInterface) Naming.lookup(name);
			c.print_on_RMIServer("RMI ON");
			System.out.println("RMI ON");
			startRMI = true;
		} catch (Exception e) {
			System.out.println("Error connecting to RMI");
			startRMI = false;
		}

	}

	public static void iniciaSockets() {
		int numero = 0;

		try {
			int serverPort = 6000;
			System.out.println("A Escuta no Porto 6000");

			//SOCKET != SERVERSOCKET
			listenSocket = new ServerSocket(serverPort);
			System.out.println("LISTEN SOCKET=" + listenSocket);

			while (true) {

				Socket clientSocket = listenSocket.accept(); // BLOQUEANTE
				System.out.println("CLIENT_SOCKET (created at accept())=" + clientSocket);
				new Connection(clientSocket, numero);
				numero++;

			}

		} catch (IOException e) {
			System.out.println("Listen:" + e.getMessage());
		}

	}

	static String toRMI(int id, String data) {
		//nao esquecer adicionar as funcoes em ambos os ServerRMIInterface.java
		String[] token = data.split("#");
		Date now;
		try {
			switch (id) {
			case 1://register
				return c.registerUser(token[0], token[1], token[2], token[3]);

			case 2://login
				return c.loginUser(token[0], token[1]);

			case 3: {//add meeting
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");

				Date dateObj = sdf.parse(token[2]);
				Date dateObj2 = sdf.parse(token[3]);

				return c.addMeeting(token[0], token[1], dateObj, dateObj2, token[4], Integer.parseInt(token[5]), token[6]);
			}
			case 4://add item
				return c.addItem(Integer.parseInt(token[0]), token[1], token[2]);

			case 5: //modify item
				return c.modifyItem(Integer.parseInt(token[0]), token[1]);

			case 6: //remove item
				return c.removeItem(Integer.parseInt(token[0]));

			case 7://add user to meeting
				return c.addUser2Meeting(Integer.parseInt(token[0]), Integer.parseInt(token[1]));

			case 8: //add action to user
				return c.addAction2User(Integer.parseInt(token[0]), Integer.parseInt(token[1]), token[2]);

			case 9://add decision to item
				return c.addDecision2Item(Integer.parseInt(token[0]), token[1]);

			case 10://print users, except the one who did the requested 
				return c.printUsers(Integer.parseInt(token[0]));

			case 11://print user's current meetings
				return c.printCurrentMeetings(Integer.parseInt(token[0]));

			case 12://print user's CURRENT meeting
				return c.printCurrentMeeting(Integer.parseInt(token[0]));

			case 13://print meeting's items
				return c.printMeetingItems(Integer.parseInt(token[0]));

			case 14://print chat from an item
				return c.printChat(Integer.parseInt(token[0]));

			case 15://print user's meetings
				return c.printUserMeetings(Integer.parseInt(token[0]));

			case 16://print user's upcoming meetings
				return c.printUpcomingMeetings(Integer.parseInt(token[0]));

			case 17://gets the chronological time of a meeting compared to current time
				return c.getMeetingCrono(Integer.parseInt(token[0]));

			case 18://print user's actions
				return c.printUserActions(Integer.parseInt(token[0]));

			case 19://print item's actions
				return c.printActions(Integer.parseInt(token[0]));

			case 20://print item's decisions
				return c.printDecisions(Integer.parseInt(token[0]));

			case 21: {//send line to chat
				return c.send2chat(Integer.parseInt(token[0]), Integer.parseInt(token[1]), token[2]);
			}
			case 22://if accepted/invited/declined meeting
				return c.getUserStatus(Integer.parseInt(token[0]), Integer.parseInt(token[1]));

			case 23://mark a task as done
				return c.actionDone(Integer.parseInt(token[0]));
			case 24://accept/decline meeting
				return c.acceptDeclineMeeting(Integer.parseInt(token[0]), Integer.parseInt(token[1]), Integer.parseInt(token[2]));
			case 25://list online users
				return c.printOnlineUsers();
			case 26://become online
				return c.becomeOnline(Integer.parseInt(token[0]));
			case 27:
				return c.becomeOffline(token[0]);
			case 28:
				return c.createGroup (token[0], Integer.parseInt(token[1]), token[2]);
			case 29:
				return c.addGroup2Meeting(Integer.parseInt(token[0]), Integer.parseInt(token[1]));
			case 30:
				return c.addUser2Group(Integer.parseInt(token[0]), Integer.parseInt(token[1]));
			case 31:
				return c.addAction2Group(Integer.parseInt(token[0]), Integer.parseInt(token[1]), token[2]);
			case 32:
				return c.acceptDeclineGroup(Integer.parseInt(token[0]), Integer.parseInt(token[1]), Integer.parseInt(token[2]));
			case 33:
				return c.printAllGroups();
			case 34:
				return c.printUserGroups(Integer.parseInt(token[0]));
			case 35:
				return c.printDetailedGroup(Integer.parseInt(token[0]));

			default:
				return "";
			}
		} catch (IOException e) {

			//System.out.println("ERRO NO SITIO CERTO?");
			startRMI = false;
			while (startRMI == false) {
				iniciaRMI();
			}

			return "";

		} catch (ParseException e) {
			e.printStackTrace();
			return "";
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}

	}

}

//= Thread para tratar de cada canal de comunicacao com um cliente
class Connection extends Thread {

	DataInputStream in;
	DataOutputStream out;
	Socket clientSocket;
	String tag, RMIresp, data;
	int thread_number;
	boolean startRMI;

	public Connection(Socket aClientSocket, int numero) {
		thread_number = numero;
		try {
			clientSocket = aClientSocket;
			in = new DataInputStream(clientSocket.getInputStream());
			out = new DataOutputStream(clientSocket.getOutputStream());
			TCPServer.connectedUsers.put(thread_number,"");

			this.start();
		} catch (IOException e) {
			System.out.println("Connection:" + e.getMessage());
		}
	}

	//=============================

	public void run() {
		String client_name;

		try {
			while (true) {
				//an echo server
				data = in.readUTF();
				if (data.equals("") || data.equals(" ") || data.equals("\n")) {
					continue;
				}

				tag = data.substring(0, 3);
				readTag(tag);

			}
		} catch (EOFException e) {  
			System.out.println("Cheguei ao run EOFexception");

			client_name = TCPServer.connectedUsers.get(thread_number);
			if (!client_name.isEmpty())
				TCPServer.toRMI(27,client_name);
			TCPServer.connectedUsers.remove(thread_number);

			System.out.println("Cliente remoto da thread #" + thread_number + " desligou-se");

		} catch (IOException e2) {
			//System.out.println("WARNING: Lost connection with RMI Server \n Trying to reconnect...");
		}
	}

	public void readTag(String tag) {

		try {
			if (tag.equals("REG")) {//register
				RMIresp = TCPServer.toRMI(1, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("LOG")) {//login      	
				RMIresp = TCPServer.toRMI(2, data.substring(4));
				System.out.println("RESPOSTA DO RMI: " + RMIresp);                	

			} else if (tag.equals("AME")) {//add meeting
				RMIresp = TCPServer.toRMI(3, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("AMI")) {//add meeting item
				RMIresp = TCPServer.toRMI(4, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("MMI")) {//modify meeting item
				RMIresp = TCPServer.toRMI(5, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("RMI")) {//remove meeting item
				RMIresp = TCPServer.toRMI(6, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("AUM")) {//add user meeting
				RMIresp = TCPServer.toRMI(7, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("AAU")) {//add action to user
				RMIresp = TCPServer.toRMI(8, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("AID")) {//add decision to item
				RMIresp = TCPServer.toRMI(9, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("PAU")) {//print all users
				RMIresp = TCPServer.toRMI(10, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("PCM")) {//print (user's) current meetings
				RMIresp = TCPServer.toRMI(11, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("PUC")) {//print user's CURRENT meeting
				RMIresp = TCPServer.toRMI(12, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("PMI")) {//print meeting items
				RMIresp = TCPServer.toRMI(13, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("PIC")) {//print chat item
				RMIresp = TCPServer.toRMI(14, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("PUM")) {//print user's meetings
				RMIresp = TCPServer.toRMI(15, data.substring(4));
				System.out.println(RMIresp);
			} else if (tag.equals("PUU")) {//print user's upcoming meetings
				RMIresp = TCPServer.toRMI(16, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("GMC")) {//get meeting crono
				RMIresp = TCPServer.toRMI(17, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("PUA")) {//print user's actions
				RMIresp = TCPServer.toRMI(18, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("PIA")) {//print item's actions
				RMIresp = TCPServer.toRMI(19, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("PID")) {//print item's decisions
				RMIresp = TCPServer.toRMI(20, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("S2C")) {//send to chat
				RMIresp = TCPServer.toRMI(21, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("GUS")) {//get user status
				RMIresp = TCPServer.toRMI(22, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("MAD")) {//mark action done
				RMIresp = TCPServer.toRMI(23, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("ADM")) {//accept decline meeting
				RMIresp = TCPServer.toRMI(24, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("LOU")) {//list online users
				RMIresp = TCPServer.toRMI(25, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("BOU")) {//become online user
				int ind = data.indexOf('#', 5);
				RMIresp = TCPServer.toRMI(26, data.substring(4,ind));
				System.out.println(RMIresp);
				TCPServer.connectedUsers.put(thread_number, data.substring(ind+1));

			} else if (tag.equals("BOF")) {//become offline user
				TCPServer.connectedUsers.put(thread_number,"");
				RMIresp = TCPServer.toRMI(27, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("CGR")) {//create group
				RMIresp = TCPServer.toRMI(28, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("AGM")) {//add group to meeting
				RMIresp = TCPServer.toRMI(29, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("AUG")) {//Add user to group
				RMIresp = TCPServer.toRMI(30, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("AAG")) {//add action to group
				RMIresp = TCPServer.toRMI(31, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("ADG")) {//accept decline group
				RMIresp = TCPServer.toRMI(32, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("PAG")) {//print all groups
				RMIresp = TCPServer.toRMI(33, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("PUG")) {//print user groups
				RMIresp = TCPServer.toRMI(34, data.substring(4));
				System.out.println(RMIresp);

			} else if (tag.equals("PDG")) {//print detailed group
				RMIresp = TCPServer.toRMI(35, data.substring(4));
				System.out.println(RMIresp);
			}
			System.out.println("T[" + thread_number + "] Recebeu: " + data);

			if (RMIresp.isEmpty() && (!tag.equals("BOF"))) {

				// System.out.println("Deu-me erro no RMI, vou repetir a leitura da tag!");
				readTag(tag);
			} else {
				sendToClientData(RMIresp);
			}

		} catch (EOFException e) {
			System.out.println("Cheguei ao readTag EOFexception");
			TCPServer.connectedUsers.remove(thread_number);
			System.out.println("Cliente remoto da thread #" + thread_number + " desligou-se");
		} catch (IOException e2) {

			System.out.println("Bye bye user #" + thread_number + " :)");

		}
	}

	//para enviar so um ok ou erro
	public void sendToClientFeedback(String data) throws IOException {
		String[] tokens = data.split("#");
		out.writeUTF(tokens[1]);
	}

	//para enviar pelo menos 2 campos
	public void sendToClientData(String data) throws IOException {
		out.writeUTF(data);
		System.out.println("ENVIEI " + data);
	} 
}

//= Thread para tratar da conexão UDP com o outro servidor (ping)
class Pings extends Thread {

	public void run() {

		UDPping udp = new UDPping(); // class where the functions who handle the UDP connections are.

		String ping = "ping";
		byte[] m = ping.getBytes();

		try {

			DatagramSocket newSocket = new DatagramSocket(6789);

			while (true) {

				byte[] buffer = new byte[1024];
				DatagramPacket feedback = new DatagramPacket(buffer, buffer.length);
				newSocket.receive(feedback); //BLOQUEANTE
				//	System.out.println(""+new String(feedback.getData(), 0, feedback.getLength()));

				DatagramPacket reply = new DatagramPacket(m,
						m.length, feedback.getAddress(), feedback.getPort());
				newSocket.send(reply);

			}

		} catch (SocketException e) {
			udp.tryToConnect();
		} catch (IOException e) {
			System.out.println("IO: " + e.getMessage());
		}

	}

}
