import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;


public interface RMIServerInterface extends Remote {
    public String registerUser (String nome, String email, String username, String password) throws RemoteException;
    public String loginUser (String username, String password) throws RemoteException;
	public String becomeOnline(int userID) throws RemoteException;	
	public String becomeOffline(String username) throws RemoteException;
	public void print_on_RMIServer(String s) throws RemoteException;
	public String addMeeting(String title, String outcome, Date startDate, Date endDate, String location, int userID, String creator) throws RemoteException, InterruptedException;
    public String addItem(int meetID, String itemName, String creator) throws RemoteException;    
    public String removeItem(int itemID) throws RemoteException;
    public String modifyItem(int itemID, String itemName) throws RemoteException;
	public String addUser2Meeting(int meetID, int user2addID) throws RemoteException;
	public String addAction2User(int itemID, int user2addID, String action) throws RemoteException;	
	public String addDecision2Item(int itemID, String decision) throws RemoteException;
	public String printUsers(int userID) throws RemoteException;
	public String printCurrentMeetings(int userID) throws RemoteException;		
	public String printCurrentMeeting(int meetID) throws RemoteException;
	public String printMeetingItems (int meetID) throws RemoteException; 
	public String printChat(int itemID) throws RemoteException;	
	public String printUserMeetings(int userID) throws RemoteException;
	public String printUpcomingMeetings(int userID) throws RemoteException;
	public String getMeetingCrono(int meetID) throws RemoteException;
	public String printUserActions(int userID) throws RemoteException;
	public String printActions(int itemID) throws RemoteException;
	public String printDecisions(int itemID) throws RemoteException;
	public String send2chat(int meetID,int itemID, String line) throws RemoteException, InterruptedException;
	public String getUserStatus(int meetID, int userID) throws RemoteException; 
	public String actionDone(int actionID) throws RemoteException;
	public String printOnlineUsers() throws RemoteException;
	public String acceptDeclineMeeting(int meetID, int userID, int option) throws RemoteException;
	
	/* Groups */
	public String createGroup (String name, int userID, String creator) throws RemoteException;
	public String addGroup2Meeting(int meetID, int group2addID) throws RemoteException;
	public String addUser2Group(int groupID, int user2addID) throws RemoteException;
	public String addAction2Group(int itemID, int group2addID, String action) throws RemoteException;
	public String acceptDeclineGroup(int groupID, int userID, int option) throws RemoteException;
	public String printAllGroups() throws RemoteException;
	public String printUserGroups(int userID) throws RemoteException;
	public String printDetailedGroup(int groupID) throws RemoteException;

}
