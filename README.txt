
	PROJECTO DE BASE DE DADOS 2014/2015

CONTEUDO DESTE ZIP
------------------------------------------------------------
* Aplica��o (codigo fonte, bibliotecas e makefile) pronto a compilar e executar o nosso software
* Ficheiro SQL pronto a importar para um DBMS (preferencialmente phpMyAdmin).
	Cont�m:tabelas, constraints,users,permitions,triggers e tudo o que � necessario ao seu bom funcionamento.
* Toda a documenta��o pedida:
	-> Manual de Utilizador
	-> Manual de Instala��o
	-> Relatorio 


COMO EXECUTAR
------------------------------------------------------------
* 1�) Importar o BD_Project.sql para o DBMS
* 2�) Correr o ficheiro "start" que se encontra na pasta da nossa aplica��o (ProjectoBD)
	
  NOTA: Para mais detalhes por favor ler o Manual de Instala��o e o Manual de Utilizador.


------------------------------------------------------------
REALIZADO POR:
	Ana Rita P�scoa n� 2010129292
	In�s Rodrigues n� 2012136686
	Jo�o Filipe Lopes n� 2012177550
------------------------------------------------------------