-- phpMyAdmin SQL Dump
-- version 4.3.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 16-Dez-2014 às 00:29
-- Versão do servidor: 5.5.40-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

-- ----------------------------------------------------- --
--							 --
--	PROJECTO DE BASE DE DADOS 2014/2015		 --
--							 --
-- 	Realizado por:					 --
--							 --
--	Ana Rita Pascoa numero: 2010129292		 --
--	Ines Afonso Rodrigues numero: 2012136686	 --
--	Joao Filipe Marques Lopes numero: 2012177550	 --
--							 --
-- ----------------------------------------------------- --

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `BD_Project`
--
-- --------------------------------------------------------

--
-- CREATE NEW USER FOR OUR PROJECT ACCESS
--

CREATE USER 'RMI'@'localhost' IDENTIFIED BY '*24E680701E5EFF5FF16AED03116D8F1A36670FAD';
GRANT SELECT, INSERT, UPDATE, DELETE ON *.* TO 'RMI'@'localhost' IDENTIFIED BY PASSWORD '*24E680701E5EFF5FF16AED03116D8F1A36670FAD';


-- --------------------------------------------------------


--
-- Estrutura da tabela `action`
--

CREATE TABLE IF NOT EXISTS `action` (
`actionid` int(10) NOT NULL,
  `userid` int(10) NOT NULL,
  `itemid` int(10) NOT NULL,
  `task` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `chat`
--

CREATE TABLE IF NOT EXISTS `chat` (
  `itemid` int(10) NOT NULL,
  `time` datetime NOT NULL,
  `message` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `decision`
--

CREATE TABLE IF NOT EXISTS `decision` (
  `itemid` int(10) NOT NULL,
  `description` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `group`
--

CREATE TABLE IF NOT EXISTS `group` (
`groupid` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `creator` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Acionadores `group`
--
DELIMITER $$
CREATE TRIGGER `user_groupTrigeer` AFTER INSERT ON `group`
 FOR EACH ROW insert into `user_group` (`userid`,`groupid`,`status`) values ((SELECT userid from `user` where username=NEW.creator),NEW.groupid,2)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `item`
--

CREATE TABLE IF NOT EXISTS `item` (
`itemid` int(10) NOT NULL,
  `meetingid` int(10) NOT NULL,
  `itemname` varchar(50) NOT NULL,
  `creator` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `meeting`
--

CREATE TABLE IF NOT EXISTS `meeting` (
`meetingid` int(10) NOT NULL,
  `title` varchar(50) NOT NULL,
  `outcome` varchar(50) NOT NULL,
  `starttime` datetime NOT NULL,
  `endtime` datetime NOT NULL,
  `location` varchar(50) NOT NULL,
  `creator` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Acionadores `meeting`
--
DELIMITER $$
CREATE TRIGGER `user_meetingTrigger` AFTER INSERT ON `meeting`
 FOR EACH ROW insert into `user_meeting` (`userid`,`meetingid`,`status`) values ((SELECT userid from `user` where username=NEW.creator),NEW.meetingid,2)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`userid` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `user`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_group`
--

CREATE TABLE IF NOT EXISTS `user_group` (
  `userid` int(10) NOT NULL,
  `groupid` int(10) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_meeting`
--

CREATE TABLE IF NOT EXISTS `user_meeting` (
  `userid` int(10) NOT NULL,
  `meetingid` int(10) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `action`
--
ALTER TABLE `action`
 ADD PRIMARY KEY (`actionid`), ADD KEY `userid` (`userid`), ADD KEY `itemid` (`itemid`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
 ADD KEY `itemid` (`itemid`);

--
-- Indexes for table `decision`
--
ALTER TABLE `decision`
 ADD KEY `itemid` (`itemid`);

--
-- Indexes for table `group`
--
ALTER TABLE `group`
 ADD PRIMARY KEY (`groupid`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
 ADD PRIMARY KEY (`itemid`), ADD KEY `meetingid` (`meetingid`);

--
-- Indexes for table `meeting`
--
ALTER TABLE `meeting`
 ADD PRIMARY KEY (`meetingid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`userid`), ADD UNIQUE KEY `username` (`username`), ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
 ADD UNIQUE KEY `ugc` (`userid`,`groupid`), ADD KEY `userid` (`userid`), ADD KEY `groupid` (`groupid`);

--
-- Indexes for table `user_meeting`
--
ALTER TABLE `user_meeting`
 ADD UNIQUE KEY `umc` (`userid`,`meetingid`), ADD KEY `userid` (`userid`), ADD KEY `meetingid` (`meetingid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `action`
--
ALTER TABLE `action`
MODIFY `actionid` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `group`
--
ALTER TABLE `group`
MODIFY `groupid` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
MODIFY `itemid` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `meeting`
--
ALTER TABLE `meeting`
MODIFY `meetingid` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `userid` int(10) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `action`
--
ALTER TABLE `action`
ADD CONSTRAINT `action_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `action_ibfk_2` FOREIGN KEY (`itemid`) REFERENCES `item` (`itemid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `chat`
--
ALTER TABLE `chat`
ADD CONSTRAINT `chat_ibfk_1` FOREIGN KEY (`itemid`) REFERENCES `item` (`itemid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `decision`
--
ALTER TABLE `decision`
ADD CONSTRAINT `decision_ibfk_1` FOREIGN KEY (`itemid`) REFERENCES `item` (`itemid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `item`
--
ALTER TABLE `item`
ADD CONSTRAINT `item_ibfk_1` FOREIGN KEY (`meetingid`) REFERENCES `meeting` (`meetingid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `user_group`
--
ALTER TABLE `user_group`
ADD CONSTRAINT `user_group_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `user_group_ibfk_2` FOREIGN KEY (`groupid`) REFERENCES `group` (`groupid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `user_meeting`
--
ALTER TABLE `user_meeting`
ADD CONSTRAINT `user_meeting_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `user_meeting_ibfk_2` FOREIGN KEY (`meetingid`) REFERENCES `meeting` (`meetingid`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
